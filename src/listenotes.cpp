#include "listenotes.h"


ListeNotes::ListeNotes(Fichier *fichierNotes, Fichier *fichierEtudiants, Fichier *fichierMatieres)
    : Fichier(FichierNotes),
      m_fichierEtudiants(fichierEtudiants),
      m_fichierMatieres(fichierMatieres),
      m_fichierNotes(fichierNotes)
{

}


ListeNotes::~ListeNotes()
{

}


bool ListeNotes::listeEstValide() const
{
    if (m_fichierEtudiants->typeFichier() != FichierEtudiants)
        return false;
    if (m_fichierMatieres->typeFichier() != FichierMatieres)
        return false;
    if (m_fichierNotes->typeFichier() != FichierNotes)
        return false;
    return true;
}


double ListeNotes::trouverNoteEtudiant(const unsigned int &numeroInscription, const unsigned int &codeMatiere) const
{
    if (!listeEstValide())
        return -1.0; // On retourne carrément une valeur négative pour signifier que le code source contient une erreur
    return m_fichierNotes->chercherValeurParTypeLigneDansEntree(codeMatiere, QString::number(numeroInscription)).toDouble();
}


double ListeNotes::calculerNotePondereeEtudiant(const unsigned int &numeroInscription, const unsigned int &codeMatiere) const
{
    if (!listeEstValide())
        return -1.0; // On retourne carrément une valeur négative pour signifier que le code source contient une erreur
    double coefficient(m_fichierMatieres->chercherValeurParTypeLigneDansEntree(codeMatiere, LigneMatiereCoefficient).toDouble());
    double note(trouverNoteEtudiant(numeroInscription, codeMatiere));
    return coefficient * note;
}

double ListeNotes::calculerTotalNotesPondereesEtudiant(const unsigned int &numeroInscription) const
{
    if (!listeEstValide())
        return -1.0; // On retourne carrément une valeur négative pour signifier que le code source contient une erreur
    double totalNotesPonderees(0.0);
    for(unsigned int i = 1 ; i <= m_fichierMatieres->trouverNombreEntrees() ; i++)
        totalNotesPonderees += calculerNotePondereeEtudiant(numeroInscription, i);

    return totalNotesPonderees;
}


double ListeNotes::calculerMoyenneGeneraleEtudiant(const unsigned int &numeroInscription) const
{
    if (!listeEstValide())
        return -1.0; // On retourne carrément une valeur négative pour signifier que le code source contient une erreur
    double sommeCoefficients(0);
    for(unsigned int i = 1 ; i <= m_fichierMatieres->trouverNombreEntrees() ; i++)
        sommeCoefficients += m_fichierMatieres->chercherValeurParTypeLigneDansEntree(i, LigneMatiereCoefficient).toDouble();
    return calculerTotalNotesPondereesEtudiant(numeroInscription) / sommeCoefficients;
}


QString ListeNotes::determinerObservationEtudiant(const unsigned int &numeroInscription) const
{
    if (!listeEstValide())
        return "ERREUR DANS LE PROGRAMME";
    double moyenneGeneraleEtudiant(calculerMoyenneGeneraleEtudiant(numeroInscription));
    if (moyenneGeneraleEtudiant >= 10.0)
        return "ADMIS";
    if (moyenneGeneraleEtudiant >= 7.5)
        return "REDOUBLANT";
    return "EXCLUS";
}

