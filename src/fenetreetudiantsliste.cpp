#include <QDate>
#include <QTableWidget>
#include <QStringList>
#include <QMessageBox>
#include "fenetreetudiantsliste.h"


FenetreEtudiantsListe::FenetreEtudiantsListe(QWidget *parent)
    : QDialog(parent), m_fichierEtudiants(new Fichier(Fichier::FichierEtudiants))
{
    setAttribute(Qt::WA_DeleteOnClose);
    setWindowTitle("Liste des étudiants du EMIT");
    resize(650, 360);

    ouvert = false;
    unsigned int nombreEntrees(m_fichierEtudiants->trouverNombreEntrees());
    if (nombreEntrees == 0)
    {
        QMessageBox::information(this, "Aucun étudiant", "Il n'y a aucun étudiant dans la liste.");
        close();
        return;
    }

    ouvert = true;

    m_titre = new Titre("Liste des étudiants du EMIT", this);

    m_filtreAnnee = new QComboBox(this);
    for(int i = 2000 ; i <= QDate::currentDate().year() ; i++)
        m_filtreAnnee->addItem(QString::number(i));
    m_filtreNiveau = new QComboBox(this);
    m_filtreNiveau->addItem("L1");
    m_filtreNiveau->addItem("L2");
    m_filtreNiveau->addItem("L3");
    m_filtreNiveau->addItem("M1");
    m_filtreNiveau->addItem("M2");
    connect(m_filtreAnnee, SIGNAL(currentIndexChanged(int)), this, SLOT(filtreMAJ()));
    connect(m_filtreNiveau, SIGNAL(currentIndexChanged(int)), this, SLOT(filtreMAJ()));
    m_loFiltre = new QVBoxLayout(nullptr);
    m_loFiltre->addWidget(m_filtreAnnee);
    m_loFiltre->addWidget(m_filtreNiveau);

    m_aide = new QLineEdit("Vous pouvez ajuster la taille de chaque colonne et de chaque ligne ainsi que la taille de cette fenêtre.", this);
    m_aide->setReadOnly(true);
    m_aide->setAlignment(Qt::AlignHCenter);

    QStringList enTetesH;
    QStringList enTetesV;
    enTetesH.push_back("N°");
    enTetesH.push_back("Nom");
    enTetesH.push_back("Adresse");
    enTetesH.push_back("Sexe");
    const unsigned int lignes(m_fichierEtudiants->trouverNombreEntrees());
    const unsigned int colonnes(4);
    for (unsigned int i = 0 ; i < lignes ; i++)
        enTetesV.push_back("");
    m_liste = new QTableWidget(lignes, colonnes, this);
    m_liste->setHorizontalHeaderLabels(enTetesH);
    m_liste->setVerticalHeaderLabels(enTetesV);
    for(unsigned int i = 0 ; i < colonnes ; i++)
    {
        for(unsigned int j = 0 ; j < lignes ; j++)
        {
            QTableWidgetItem *item(new QTableWidgetItem);
            m_liste->setItem(j, i, item);
        }
    }

    m_loListe = new QVBoxLayout(this);
    m_loListe->addWidget(m_titre);
    m_loListe->addLayout(m_loFiltre);
    m_loListe->addWidget(m_aide);
    m_loListe->addWidget(m_liste);
}


FenetreEtudiantsListe::~FenetreEtudiantsListe()
{
    delete m_fichierEtudiants;
}

void FenetreEtudiantsListe::filtreMAJ()
{
    if (!ouvert)
        return;
    QString annee(m_filtreAnnee->currentText());
    QString niveau(m_filtreNiveau->currentText());
    QList<unsigned int> etudiantsDansAnnee(m_fichierEtudiants->listeEntreesParTypeEtValeurLigne(Fichier::LigneEtudiantAnnee, annee));
    QList<unsigned int> etudiantsDansNiveau(m_fichierEtudiants->listeEntreesParTypeEtValeurLigne(Fichier::LigneEtudiantNiveau, niveau));
    QList<unsigned int> etudiantsDansAnneeEtNiveau;
    for(unsigned int i = 0 ; i < (unsigned int)etudiantsDansAnnee.size() ; i++)
    {
        unsigned int etudiant(etudiantsDansAnnee.at(i));
        if (etudiantsDansNiveau.contains(etudiant))
            etudiantsDansAnneeEtNiveau.push_back(etudiant);
    }

    unsigned int k(0);
    for(unsigned int i = 0 ; i < (unsigned int)m_liste->rowCount() ; i++)
    {
        for(unsigned int j = 0 ; j < (unsigned int)m_liste->columnCount() ; j++)
        {
            QTableWidgetItem *item(m_liste->item(i, j));
            item->setText("");
            if (k >= (unsigned int)etudiantsDansAnneeEtNiveau.size())
                continue;
            unsigned int numeroInscription(etudiantsDansAnneeEtNiveau.at(k));
            if (j == 0) // Numéro d'inscription
            {
                item->setText(QString::number(numeroInscription));
            }
            else if (j == 1) // Nom
            {
                QString nom(m_fichierEtudiants->chercherValeurParTypeLigneDansEntree(numeroInscription, Fichier::LigneEtudiantNom));
                item->setText(nom);
            }
            else if (j == 2) // Adresse
            {
                QString adresse(m_fichierEtudiants->chercherValeurParTypeLigneDansEntree(numeroInscription, Fichier::LigneEtudiantAdresse));
                item->setText(adresse);
            }
            else if (j == 3) // Sexe
            {
                QString sexe(m_fichierEtudiants->chercherValeurParTypeLigneDansEntree(numeroInscription, Fichier::LigneEtudiantSexe));
                item->setText(sexe);
                k++;
            }
        }
    }
}

void FenetreEtudiantsListe::showEvent(QShowEvent *)
{
    filtreMAJ();
}
