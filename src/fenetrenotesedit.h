#ifndef FENETRENOTESEDIT_H
#define FENETRENOTESEDIT_H


#include <QDialog>
#include <QLabel>
#include <QComboBox>
#include <QSpinBox>
#include <QLineEdit>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include "widgets.h"
#include "fichier.h"
#include <QShowEvent>


class FenetreNotesEdit : public QDialog
{
    Q_OBJECT

public:
    FenetreNotesEdit(QWidget *parent = nullptr);
    ~FenetreNotesEdit();

private slots:
    void filtresMAJ();
    void ajouterNote();
    void modifierNote();
    void supprimerNote();

private:
    Fichier *m_fichierEtudiants;
    Fichier *m_fichierMatieres;
    Fichier *m_fichierNotes;

    Titre *m_titre;

    QComboBox *m_editMode;
    QLabel *m_texte1;
    QComboBox *m_editMatiere;
    QLabel *m_texte2;
    QSpinBox *m_editEtudiant;
    QLabel *m_texte3;
    QLineEdit *m_editNote;
    QLabel *m_texte4;
    BoutonMinMin *m_editAction;
    LineEditEnLectureSeule *m_rappelNomEtudiant;
    QHBoxLayout *m_loFormulaire;
    QVBoxLayout *m_loPrincipal;

    LineEditEnLectureSeule *m_editNotification;

    void showEvent(QShowEvent*);
};


#endif // !FENETRENOTESEDIT_H
