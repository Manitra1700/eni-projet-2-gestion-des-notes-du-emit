#include <QDate>
#include <QMessageBox>
#include "fenetreetudiantsmodif.h"


FenetreEtudiantsModif::FenetreEtudiantsModif(QWidget *parent)
    : QDialog(parent), m_fichierEtudiants(new Fichier(Fichier::FichierEtudiants))
{
    setAttribute(Qt::WA_DeleteOnClose);
    setWindowTitle("Modification d'un étudiant");
    resize(650, 360);

    m_titre = new Titre("Modification d'un étudiant", this);

    unsigned int dernierNumeroInscription(m_fichierEtudiants->trouverNombreEntrees());

    m_wfNumeroInscription = new QSpinBox(this);
    m_wfNumeroInscription->setMinimum(1);
    m_wfNumeroInscription->setMaximum(dernierNumeroInscription);
    connect(m_wfNumeroInscription, SIGNAL(valueChanged(int)), this, SLOT(numeroMAJ()));

    m_wfNom = new QLineEdit(this);
    m_wfAdresse = new QLineEdit(this);

    m_wfSexe = new QComboBox(this);
    m_wfSexe->addItem("Masculin");
    m_wfSexe->addItem("Féminin");

    m_wfNiveau = new QComboBox(this);
    m_wfNiveau->addItem("L1");
    m_wfNiveau->addItem("L2");
    m_wfNiveau->addItem("L3");
    m_wfNiveau->addItem("M1");
    m_wfNiveau->addItem("M2");

    m_wfAnnee = new QComboBox(this);
    for(auto i = 2000 ; i <= QDate::currentDate().year() ; i++)
        m_wfAnnee->addItem(QString::number(i));

    m_waModifier = new BoutonMinMin("Enregistrer", this);
    connect(m_waModifier, SIGNAL(clicked()), this, SLOT(modifierEtudiant()));
    m_waAnnuler = new BoutonMinMin("Annuler", this);
    connect(m_waAnnuler, SIGNAL(clicked()), this, SLOT(close()));

    m_loFormulaireEtudiantsModif = new QFormLayout(this);
    m_loFormulaireEtudiantsModif->addWidget(m_titre);
    m_loFormulaireEtudiantsModif->addRow("Numéro d'inscription", m_wfNumeroInscription);
    m_loFormulaireEtudiantsModif->addRow("Nom", m_wfNom);
    m_loFormulaireEtudiantsModif->addRow("Adressse", m_wfAdresse);
    m_loFormulaireEtudiantsModif->addRow("Sexe", m_wfSexe);
    m_loFormulaireEtudiantsModif->addRow("Niveau", m_wfNiveau);
    m_loFormulaireEtudiantsModif->addRow("Année", m_wfAnnee);
    m_loFormulaireEtudiantsModif->addWidget(m_waModifier);
    m_loFormulaireEtudiantsModif->addWidget(m_waAnnuler);
}


FenetreEtudiantsModif::~FenetreEtudiantsModif()
{
    delete m_fichierEtudiants;
}


void FenetreEtudiantsModif::numeroMAJ()
{
    // i pour input - il s'agit du numéro saisi dans le spin box
    unsigned int iNumero(m_wfNumeroInscription->value());
    // f pour fichier - on va chercher dans le fichier si la valeur de iNumero a une ligne de nom
    // et on met ce nom dans fNomCorrespondant. La méthode retourne "" s'il n'y a pas de nom (étudiant supprimé ou non inscrit)
    QString fNomCorrespondant(m_fichierEtudiants->chercherValeurParTypeLigneDansEntree(iNumero, Fichier::LigneEtudiantNom));
    if (m_fichierEtudiants->entreeEstDefinie(iNumero))
    {
        if (fNomCorrespondant == "")
        {
            m_wfNom->setText("Etudiant supprimé");
            m_wfAdresse->setText("");
            m_wfSexe->setCurrentIndex(0);
            m_wfNiveau->setCurrentIndex(0);
            m_wfAnnee->setCurrentIndex(0);
        }
        else
        {
            m_wfNom->setText(fNomCorrespondant);
            m_wfAdresse->setText(m_fichierEtudiants->chercherValeurParTypeLigneDansEntree(iNumero, Fichier::LigneEtudiantAdresse));
            m_wfSexe->setCurrentText(m_fichierEtudiants->chercherValeurParTypeLigneDansEntree(iNumero, Fichier::LigneEtudiantSexe));
            m_wfNiveau->setCurrentText(m_fichierEtudiants->chercherValeurParTypeLigneDansEntree(iNumero, Fichier::LigneEtudiantNiveau));
            m_wfAnnee->setCurrentText(m_fichierEtudiants->chercherValeurParTypeLigneDansEntree(iNumero, Fichier::LigneEtudiantAnnee));
        }
    }
    else
    {
        m_wfNom->setText("");
        m_wfAdresse->setText("");
        m_wfSexe->setCurrentIndex(0);
        m_wfNiveau->setCurrentIndex(0);
        m_wfAnnee->setCurrentIndex(0);
    }
}


void FenetreEtudiantsModif::modifierEtudiant()
{
    // i pour input - il s'agit du numéro saisi dans le spin box
    unsigned int iNumero(m_wfNumeroInscription->value());
    if (!m_fichierEtudiants->entreeEstDefinie(iNumero))
    {
        // Normalement, ceci ne devrait jamais arriver.
        QMessageBox::warning(this, "Numéro non attribué",
                             "Le numéro " + QString::number(iNumero) + " n'est attribué à aucun étudiant dans la liste des étudiants."
                             "\nSi vous souhaitez ajouter un nouvel étudiant, allez plutôt dans l'option 'Ajout d'un nouvel étudiant'.");
        return;
    }

    QString iNom(m_wfNom->text());
    if (iNom == "")
    {
        QMessageBox::warning(this, "Nom requis",
                             "Le champ 'Nom' est vide. Veuillez le remplir s'il vous plaît.");
        return;
    }

    QString iAdresse(m_wfAdresse->text());
    if (iAdresse == "")
    {
        QMessageBox::warning(this, "Adresse requise",
                             "Le champ 'Adresse' est vide. Veuillez le remplir s'il vous plaît.");
        return;
    }

    QString iSexe(m_wfSexe->currentText());
    QString iNiveau(m_wfNiveau->currentText());
    QString iAnnee(m_wfAnnee->currentText());

    m_fichierEtudiants->modifierLigne(iNumero, Fichier::LigneEtudiantNom, iNom);
    m_fichierEtudiants->modifierLigne(iNumero, Fichier::LigneEtudiantAdresse, iAdresse);
    m_fichierEtudiants->modifierLigne(iNumero, Fichier::LigneEtudiantSexe, iSexe);
    m_fichierEtudiants->modifierLigne(iNumero, Fichier::LigneEtudiantNiveau, iNiveau);
    m_fichierEtudiants->modifierLigne(iNumero, Fichier::LigneEtudiantAnnee, iAnnee);

    QMessageBox::information(this, "Modification effectuée", "Les informations sur " + iNom +
                                        " ont été mises à jour avec succès.");

    if (iNumero < (unsigned int)m_wfNumeroInscription->maximum())
        m_wfNumeroInscription->setValue(iNumero + 1);
    else
        close();
}


void FenetreEtudiantsModif::showEvent(QShowEvent*)
{
    numeroMAJ();
}
