#ifndef FENETREMATIERESSUPPR_H
#define FENETREMATIERESSUPPR_H


#include <QDialog>
#include <QLabel>
#include <QSpinBox>
#include <QLineEdit>
#include <QComboBox>
#include "widgets.h"
#include <QFormLayout>
#include <QShowEvent>

#include "fichier.h"


class FenetreMatieresSuppr : public QDialog
{
    Q_OBJECT

public:
    FenetreMatieresSuppr(QWidget *parent = nullptr);
    ~FenetreMatieresSuppr();

private slots:
    void codeMAJ();
    void supprimerMatiere();

private:
    Fichier *m_fichierMatieres;

    Titre *m_titre;

    // wf pour "widget formularire"
    QSpinBox *m_wfCodeMatiere;
    // ls pour "lecture seule"
    LineEditEnLectureSeule *m_lsDesignation;
    LineEditEnLectureSeule *m_lsCoefficient;

    // wa pour "widget action"
    BoutonMinMin *m_waSupprimer;
    BoutonMinMin *m_waAnnuler;

    // lo pour "layout"
    QFormLayout *m_loFormulaireMatieresSuppr;

    void showEvent(QShowEvent*);
};


#endif // !FENETREMATIERESSUPPR_H
