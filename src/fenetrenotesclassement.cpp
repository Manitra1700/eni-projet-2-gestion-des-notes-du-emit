#include <QApplication>
#include <QDate>
#include <QList>
#include <QMap>
#include <QTextDocument>
#include <QPrinter>
#include <QDir>
#include <QFile>
#include <QWebChannel>
#include "fenetrenotesclassement.h"


FenetreNotesClassement::FenetreNotesClassement(QWidget *parent)
    : QDialog(parent),
      m_fichierEtudiant(new Fichier(Fichier::FichierEtudiants)),
      m_fichierMatieres(new Fichier(Fichier::FichierMatieres)),
      m_fichierNotes(new Fichier(Fichier::FichierNotes)),
      m_listeNotes(new ListeNotes(m_fichierNotes, m_fichierEtudiant, m_fichierMatieres))
{
    setAttribute(Qt::WA_DeleteOnClose);
    setWindowTitle("Classement par ordre de mérite");
    setFixedHeight(130);
    setMinimumWidth(295);

    m_filtreAnnee = new QComboBox(this);
    for(int i = 2000 ; i <= QDate::currentDate().year() ; i++)
        m_filtreAnnee->addItem(QString::number(i));
    m_filtreNiveau = new QComboBox(this);
    m_filtreNiveau->addItem("L1");
    m_filtreNiveau->addItem("L2");
    m_filtreNiveau->addItem("L3");
    m_filtreNiveau->addItem("M1");
    m_filtreNiveau->addItem("M2");
    m_actionAfficherApercu = new QPushButton("Afficher aperçu", this);
    connect(m_actionAfficherApercu, SIGNAL(clicked()), this, SLOT(afficherApercu()));
    m_actionFermer = new QPushButton("Retour", this);
    connect(m_actionFermer, SIGNAL(clicked()), this, SLOT(close()));

    m_loFiltre = new QFormLayout(this);
    m_loFiltre->addRow("Année", m_filtreAnnee);
    m_loFiltre->addRow("Niveau", m_filtreNiveau);
    m_loFiltre->addWidget(m_actionAfficherApercu);
    m_loFiltre->addWidget(m_actionFermer);
}


FenetreNotesClassement::~FenetreNotesClassement()
{
    delete m_fichierEtudiant;
    delete m_fichierMatieres;
    delete m_fichierNotes;
    delete m_listeNotes;
    delete m_apercuOuvert;
}


void FenetreNotesClassement::desactiverBoutonsActions()
{
    m_actionAfficherApercu->setVisible(false);
    m_actionFermer->setVisible(false);
}


void FenetreNotesClassement::activerBoutonsActions()
{
    m_actionAfficherApercu->setVisible(true);
    m_actionFermer->setVisible(true);
}


void FenetreNotesClassement::changerEtatApercu(bool ouvert)
{
    *m_apercuOuvert = ouvert;
}


bool FenetreNotesClassement::etatApercu() const
{
    return *m_apercuOuvert;
}


void FenetreNotesClassement::afficherApercu()
{
    QString annee(m_filtreAnnee->currentText());
    QString niveau(m_filtreNiveau->currentText());
    QList<unsigned int> listeEtudiantsDansAnnee(
                m_fichierEtudiant->listeEntreesParTypeEtValeurLigne(Fichier::LigneEtudiantAnnee, annee)
                );
    QList<unsigned int> listeEtudiantsDansNiveau(
                m_fichierEtudiant->listeEntreesParTypeEtValeurLigne(Fichier::LigneEtudiantNiveau, niveau)
                );
    QList<unsigned int> listeEtudiantsDansAnneeEtNiveau;
    for(unsigned int i = 0 ; i < (unsigned int)listeEtudiantsDansAnnee.size() ; i++)
    {
        unsigned int etudiant(listeEtudiantsDansAnnee.at(i));
        if (listeEtudiantsDansNiveau.contains(listeEtudiantsDansAnnee.at(i)))
            listeEtudiantsDansAnneeEtNiveau.push_back(etudiant);
    }
    listeEtudiantsDansAnnee.clear();
    listeEtudiantsDansNiveau.clear();

    QMap<unsigned int, double> listeEtudiantsAvecMoyenne;
    for(unsigned int i = 0 ; i < (unsigned int)listeEtudiantsDansAnneeEtNiveau.size() ; i++)
    {
        unsigned int etudiant(listeEtudiantsDansAnneeEtNiveau.at(i));
        double moyenne(m_listeNotes->calculerMoyenneGeneraleEtudiant(etudiant));
        listeEtudiantsAvecMoyenne.insert(etudiant, moyenne);
    }

    QString tableauMoyenne(
"<tr>"
    "<td style=\"background-color: #878\">!numero!</td>"
    "<td style=\"background-color: #cdc\">!nom!</td>"
    "<td style=\"background-color: #878\">!moyenne!</td>"
"</tr>"
    );

    QString modeleHtml(
"<html lang=\"fr\">"
    "<body>"
        "<h1 align=\"center\">CLASSEMENT PAR ORDRE DE MERITE</h1>"
        "<b>ANNEE</b> : !annee! | <b>NIVEAU</b> : !niveau!<br/>"
        "<table style=\"vertical-align: middle\">"
            "<thead>"
                "<tr>"
                    "<th style=\"background-color: #878\">N° ETUDIANT</th>"
                    "<th style=\"background-color: #cdc\">NOM</th>"
                    "<th style=\"background-color: #878\">MOYENNE</th>"
                "</tr>"
            "</thead>"
            "<tbody>"
                "!tableaumoyenne!"
            "</tbody>"
        "</table>"
    "</body>"
"</html>"
    );

    QTextDocument modele;
    QPrinter printer(QPrinter::PrinterResolution);
    printer.setOutputFormat(QPrinter::PdfFormat);
    printer.setPaperSize(QPrinter::A4);

    QString app_path(qApp->applicationDirPath());
#ifdef Q_OS_MACOS
    QDir app_path_dir(app_path);
    app_path_dir.cdUp();
    app_path_dir.cdUp();
    app_path_dir.cdUp();
    app_path = app_path_dir.absolutePath();
#endif
    QDir dir(app_path + "/minified/web/");
    QString pdf_path(dir.relativeFilePath("classement.pdf"));
    printer.setOutputFileName(dir.absoluteFilePath("classement.pdf"));
    QFile fichierPdf(dir.absoluteFilePath("classement.pdf"));

    QString tableauMoyenneFinal("");
    while(!listeEtudiantsAvecMoyenne.isEmpty())
    {
        QList<unsigned int> listeEtudiants(listeEtudiantsAvecMoyenne.keys());
        double meilleureMoyenne(0);
        unsigned int meilleurEtudiant(-1);
        for(unsigned int i = 0 ; i < (unsigned int)listeEtudiants.size() ; i++)
        {
            unsigned int iEtudiant(listeEtudiants.at(i));
            double moyenne(m_listeNotes->calculerMoyenneGeneraleEtudiant(iEtudiant));
            if (meilleureMoyenne < moyenne)
            {
                meilleureMoyenne = moyenne;
                meilleurEtudiant = iEtudiant;
            }
        }
        if (meilleurEtudiant == -1)
            break;
        listeEtudiantsAvecMoyenne.remove(meilleurEtudiant);

        QString numeroEtudiant(QString::number(meilleurEtudiant));
        QString nomEtudiant(m_fichierEtudiant->chercherValeurParTypeLigneDansEntree(meilleurEtudiant,
                                                                                    Fichier::LigneEtudiantNom));
        QString moyenneEtudiant(QString::number(meilleureMoyenne));
        QString copieTableauMoyenne(tableauMoyenne);
        copieTableauMoyenne.replace(QRegularExpression("!numero!"), numeroEtudiant);
        copieTableauMoyenne.replace(QRegularExpression("!nom!"), nomEtudiant);
        copieTableauMoyenne.replace(QRegularExpression("!moyenne!"), moyenneEtudiant);
        tableauMoyenneFinal += copieTableauMoyenne;
    }

    modeleHtml.replace(QRegularExpression("!annee!"), annee);
    modeleHtml.replace(QRegularExpression("!niveau!"), niveau);
    modeleHtml.replace(QRegularExpression("!tableaumoyenne!"), tableauMoyenneFinal);

    modele.setHtml(modeleHtml);
    fichierPdf.remove();
    modele.print(&printer);

    FenetreApercuClassement *fenetreApercu(new FenetreApercuClassement(this, this));
    auto url(QUrl::fromLocalFile(app_path + "/minified/web/viewer.html"));

    m_communicator = new Communicator(fenetreApercu);
    m_communicator->setUrl(pdf_path);

    m_webView = new QWebEngineView(fenetreApercu);

    QWebChannel *channel(new QWebChannel(fenetreApercu));
    channel->registerObject(QStringLiteral("communicator"), m_communicator);
    m_webView->page()->setWebChannel(channel);

    m_webView->load(url);
    fenetreApercu->setCentralWidget(m_webView);
    fenetreApercu->resize(640, 480);
    fenetreApercu->show();
}


void FenetreNotesClassement::closeEvent(QCloseEvent *event)
{
    if (*m_apercuOuvert)
        event->ignore();
}


FenetreApercuClassement::FenetreApercuClassement(QWidget *parent, FenetreNotesClassement *parentFenetre)
    : QMainWindow(parent),
      m_parentFenetre(parentFenetre)
{

}


FenetreApercuClassement::~FenetreApercuClassement()
{

}


void FenetreApercuClassement::showEvent(QShowEvent*)
{
    m_parentFenetre->desactiverBoutonsActions();
    m_parentFenetre->changerEtatApercu(true);
}


void FenetreApercuClassement::closeEvent(QCloseEvent*)
{
    m_parentFenetre->activerBoutonsActions();
    m_parentFenetre->changerEtatApercu(false);
}

