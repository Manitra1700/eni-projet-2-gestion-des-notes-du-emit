#ifndef FENETREMATIERESMODIF_H
#define FENETREMATIERESMODIF_H


#include <QDialog>
#include <QLabel>
#include <QSpinBox>
#include <QLineEdit>
#include <QComboBox>
#include "widgets.h"
#include <QFormLayout>
#include <QShowEvent>

#include "fichier.h"


class FenetreMatieresModif : public QDialog
{
    Q_OBJECT

public:
    FenetreMatieresModif(QWidget *parent = nullptr);
    ~FenetreMatieresModif();

private slots:
    void codeMAJ();
    void modifierMatiere();

private:
    Fichier *m_fichierMatieres;

    Titre *m_titre;

    // wf pour "widget formularire"
    QSpinBox *m_wfCodeMatiere;
    QLineEdit *m_wfDesignation;
    QLineEdit *m_wfCoefficient;

    // wa pour "widget action"
    BoutonMinMin *m_waModifier;
    BoutonMinMin *m_waAnnuler;

    // lo pour "layout"
    QFormLayout *m_loFormulaireMatieresModif;

    void showEvent(QShowEvent*);
};


#endif // !FENETREMATIERESMODIF_H
