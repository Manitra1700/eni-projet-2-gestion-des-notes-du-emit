#include <QMessageBox>
#include <QList>
#include "fenetrenotesedit.h"


FenetreNotesEdit::FenetreNotesEdit(QWidget *parent)
    : QDialog(parent),
      m_fichierEtudiants(new Fichier(Fichier::FichierEtudiants)),
      m_fichierMatieres(new Fichier(Fichier::FichierMatieres)),
      m_fichierNotes(new Fichier(Fichier::FichierNotes))
{
    setAttribute(Qt::WA_DeleteOnClose);
    setWindowTitle("Edition des notes");
    setFixedHeight(130);

    if (m_fichierEtudiants->trouverNombreEntrees() == 0)
    {
        QMessageBox::warning(this, "Aucun étudiant",
                             "Il n'y a aucun étudiant dans la liste des étudiants.");
        close();
        return;
    }

    if (m_fichierMatieres->trouverNombreEntrees() == 0)
    {
        QMessageBox::warning(this, "Aucune EC",
                             "Il n'y a aucune EC dans la liste des EC.");
        close();
        return;
    }

    m_titre = new Titre("Edition des notes", this);

    m_editMode = new QComboBox(this);
    m_editMode->addItem("Ajout");
    m_editMode->addItem("Modification");
    m_editMode->addItem("Suppression");
    connect(m_editMode, SIGNAL(currentIndexChanged(int)), this, SLOT(filtresMAJ()));
    m_texte1 = new QLabel(" de note de ", this);

    m_editMatiere = new QComboBox(this);
    QList<unsigned int> matieres(m_fichierMatieres->listeEntrees());
    for(auto i = 0 ; i < matieres.size() ; i++)
    {
        QString designation(m_fichierMatieres->chercherValeurParTypeLigneDansEntree(matieres.at(i), Fichier::LigneMatiereNom));
        if (designation == "")
            continue;
        m_editMatiere->addItem(designation);
    }
    connect(m_editMatiere, SIGNAL(currentIndexChanged(int)), this, SLOT(filtresMAJ()));

    m_texte2 = new QLabel(" de l'étudiant ", this);

    m_editEtudiant = new QSpinBox(this);
    m_editEtudiant->setMinimum(1);
    m_editEtudiant->setMaximum(m_fichierEtudiants->trouverNombreEntrees());
    connect(m_editEtudiant, SIGNAL(valueChanged(int)), this, SLOT(filtresMAJ()));

    m_texte3 = new QLabel(" : ", this);

    m_editNote = new QLineEdit(this);

    m_texte4 = new QLabel(" / 20 ", this);

    m_editAction = new BoutonMinMin(this);
    connect(m_editAction, SIGNAL(clicked()), this, SLOT(modifierNote()));
    connect(m_editAction, SIGNAL(clicked()), this, SLOT(supprimerNote()));
    connect(m_editAction, SIGNAL(clicked()), this, SLOT(ajouterNote()));

    m_rappelNomEtudiant = new LineEditEnLectureSeule(this, false);
    m_editNotification = new LineEditEnLectureSeule(this, false);

    m_loFormulaire = new QHBoxLayout(nullptr);
    m_loFormulaire->addWidget(m_editMode);
    m_loFormulaire->addWidget(m_texte1);
    m_loFormulaire->addWidget(m_editMatiere);
    m_loFormulaire->addWidget(m_texte2);
    m_loFormulaire->addWidget(m_editEtudiant);
    m_loFormulaire->addWidget(m_texte3);
    m_loFormulaire->addWidget(m_editNote);
    m_loFormulaire->addWidget(m_texte4);
    m_loFormulaire->addWidget(m_editAction);

    m_loPrincipal = new QVBoxLayout(this);
    m_loPrincipal->addWidget(m_titre);
    m_loPrincipal->addLayout(m_loFormulaire);
    m_loPrincipal->addWidget(m_rappelNomEtudiant);
    m_loPrincipal->addWidget(m_editNotification);
}


FenetreNotesEdit::~FenetreNotesEdit()
{
    delete m_fichierEtudiants;
    delete m_fichierMatieres;
    delete m_fichierNotes;
}


void FenetreNotesEdit::filtresMAJ()
{
    unsigned int etudiant(m_editEtudiant->value());
    if (m_fichierEtudiants->chercherValeurParTypeLigneDansEntree(etudiant, Fichier::LigneEtudiantNom) == "")
    {
        m_editAction->setVisible(false);
        m_editNote->setText("Etudiant supprimé ou non inscrit");
    }
    else
    {
        m_editAction->setVisible(true);
        QString mode(m_editMode->currentText());
        if (mode == "Ajout")
        {
            m_editAction->setText("Ajouter");
        }
        else if (mode == "Modification")
        {
            m_editAction->setText("Modifier");
        }
        else if (mode == "Suppression")
        {
            m_editAction->setText("Supprimer");
        }

        unsigned int matiere(m_fichierMatieres->listeEntreesParTypeEtValeurLigne(Fichier::LigneMatiereNom, m_editMatiere->currentText()).at(0));
        QString nomEtudiant(m_fichierEtudiants->chercherValeurParTypeLigneDansEntree(etudiant, Fichier::LigneEtudiantNom));
        m_rappelNomEtudiant->setText(nomEtudiant);
        QString noteEtudiant(m_fichierNotes->chercherValeurParTypeLigneDansEntree(matiere, QString::number(etudiant)));
        m_editNote->setText(noteEtudiant);
    }
}


void FenetreNotesEdit::ajouterNote()
{
    if (m_editMode->currentText() != "Ajout")
        return;
    // i pour input -- Il s'agit de valeurs extraites à partir des saisies de l'utilisateur sur le programme en cours d'exécution
    unsigned int iEtudiant(m_editEtudiant->value());
    QString iDesignationMatiere(m_editMatiere->currentText());
    QString iNoteEtudiant(m_editNote->text());
    // f pour file ou fichier -- Il s'agit de valeurs extraites à partir des fichiers listes
    unsigned int fMatiere(m_fichierMatieres->listeEntreesParTypeEtValeurLigne(Fichier::LigneMatiereNom, iDesignationMatiere).at(0));
    QString fNomEtudiant(m_fichierEtudiants->chercherValeurParTypeLigneDansEntree(iEtudiant, Fichier::LigneEtudiantNom));
    QString fNoteEtudiant(m_fichierNotes->chercherValeurParTypeLigneDansEntree(fMatiere, QString::number(iEtudiant)));
    if (m_fichierNotes->listeEntreesParTypeEtValeurLigne(QString::number(iEtudiant), "\\d+\\.{0,1}[\\d]*").contains(fMatiere))
    {
        QMessageBox::warning(this, "Déjà noté", fNomEtudiant + " a " + fNoteEtudiant + "/20 en " + iDesignationMatiere);
    }
    else
    {
        if (m_fichierNotes->ligneEstDefinie(fMatiere, QString::number(iEtudiant)))
        {
            modifierNote();
        }
        if (iNoteEtudiant.toDouble() > 20)
        {
            QMessageBox::warning(this, "Note invalide", "La note maximale est de 20. La note pondérée sera calculée par ce programme.");
        }
        else if (iNoteEtudiant.toDouble() < 0)
        {
            QMessageBox::warning(this, "Note invalide", "La note minimale est de 0.");
        }
        else
        {
            m_fichierNotes->nouvelleLigne(fMatiere, QString::number(iEtudiant), iNoteEtudiant);
            m_editNotification->setText("Enregistré : " + iNoteEtudiant + "/20 en " + iDesignationMatiere + " pour " +
                                        fNomEtudiant);
            if (m_editEtudiant->value() < m_editEtudiant->maximum())
                m_editEtudiant->setValue(m_editEtudiant->value() + 1);
        }
    }
}


void FenetreNotesEdit::modifierNote()
{
    if (m_editMode->currentText() != "Modification")
        return;
    // i pour input -- Il s'agit de valeurs extraites à partir des saisies de l'utilisateur sur le programme en cours d'exécution
    unsigned int iEtudiant(m_editEtudiant->value());
    QString iDesignationMatiere(m_editMatiere->currentText());
    QString iNoteEtudiant(m_editNote->text());
    // f pour file ou fichier -- Il s'agit de valeurs extraites à partir des fichiers listes
    unsigned int fMatiere(m_fichierMatieres->listeEntreesParTypeEtValeurLigne(Fichier::LigneMatiereNom, iDesignationMatiere).at(0));
    QString fNomEtudiant(m_fichierEtudiants->chercherValeurParTypeLigneDansEntree(iEtudiant, Fichier::LigneEtudiantNom));
    if (!m_fichierNotes->listeEntreesParTypeEtValeurLigne(QString::number(iEtudiant), "\\d+\\.{0,1}[\\d]*").contains(fMatiere) &&
            !m_fichierNotes->ligneEstDefinie(fMatiere, QString::number(iEtudiant)))
    {
        QMessageBox::warning(this, "Pas de note", fNomEtudiant + " n'a pas de note en " + iDesignationMatiere);
    }
    else
    {
        if (iNoteEtudiant.toDouble() > 20)
        {
            QMessageBox::warning(this, "Note invalide", "La note maximale est de 20. La note pondérée sera calculée par ce programme.");
        }
        else if (iNoteEtudiant.toDouble() < 0)
        {
            QMessageBox::warning(this, "Note invalide", "La note minimale est de 0.");
        }
        else
        {
            m_fichierNotes->modifierLigne(fMatiere, QString::number(iEtudiant), iNoteEtudiant);
            m_editNotification->setText("Enregistré : " + iNoteEtudiant + "/20 en " + iDesignationMatiere + " pour " +
                                        fNomEtudiant);
            if (m_editEtudiant->value() < m_editEtudiant->maximum())
                m_editEtudiant->setValue(m_editEtudiant->value() + 1);
        }
    }
}


void FenetreNotesEdit::supprimerNote()
{
    if (m_editMode->currentText() != "Suppression")
        return;
    // i pour input -- Il s'agit de valeurs extraites à partir des saisies de l'utilisateur sur le programme en cours d'exécution
    unsigned int iEtudiant(m_editEtudiant->value());
    QString iDesignationMatiere(m_editMatiere->currentText());
    // f pour file ou fichier -- Il s'agit de valeurs extraites à partir des fichiers listes
    unsigned int fMatiere(m_fichierMatieres->listeEntreesParTypeEtValeurLigne(Fichier::LigneMatiereNom, iDesignationMatiere).at(0));
    QString fNomEtudiant(m_fichierEtudiants->chercherValeurParTypeLigneDansEntree(iEtudiant, Fichier::LigneEtudiantNom));
    if (!m_fichierNotes->listeEntreesParTypeEtValeurLigne(QString::number(iEtudiant), "\\d+\\.{0,1}[\\d]*").contains(fMatiere) &&
            !m_fichierNotes->ligneEstDefinie(fMatiere, QString::number(iEtudiant)))
    {
        QMessageBox::warning(this, "Pas de note", fNomEtudiant + " n'a pas de note en " + iDesignationMatiere);
    }
    else
    {
        m_fichierNotes->supprimerLigne(fMatiere, QString::number(iEtudiant));
        m_editNotification->setText("Supprimé : note de " + iDesignationMatiere + " pour " + fNomEtudiant);
        if (m_editEtudiant->value() < m_editEtudiant->maximum())
            m_editEtudiant->setValue(m_editEtudiant->value() + 1);
    }
}


void FenetreNotesEdit::showEvent(QShowEvent *)
{
    filtresMAJ();
}

