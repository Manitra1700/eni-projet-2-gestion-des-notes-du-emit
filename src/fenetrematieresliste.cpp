#include <QDate>
#include <QTableWidget>
#include <QStringList>
#include <QMessageBox>
#include "fenetrematieresliste.h"


FenetreMatieresListe::FenetreMatieresListe(QWidget *parent)
    : QDialog(parent), m_fichierMatieres(new Fichier(Fichier::FichierMatieres))
{
    setAttribute(Qt::WA_DeleteOnClose);
    setWindowTitle("Listage des EC");
    resize(650, 360);

    unsigned int nombreEntrees(m_fichierMatieres->trouverNombreEntrees());
    if (nombreEntrees == 0)
    {
        QMessageBox::information(this, "Aucune EC", "Il n'y a aucune EC dans la liste.");
        close();
        return;
    }

    m_titre = new Titre("Listage des EC", this);

    QStringList enTetesH;
    QStringList enTetesV;
    enTetesH.push_back("Code");
    enTetesH.push_back("Désignation");
    enTetesH.push_back("Coefficient");
    const unsigned int lignes(m_fichierMatieres->trouverNombreEntrees());
    const unsigned int colonnes(3);
    for (unsigned int i = 0 ; i < lignes ; i++)
        enTetesV.push_back("");
    m_liste = new QTableWidget(lignes, colonnes, this);
    m_liste->setHorizontalHeaderLabels(enTetesH);
    m_liste->setVerticalHeaderLabels(enTetesV);
    QList<unsigned int> matieresValides;
    QList<unsigned int> matieres(m_fichierMatieres->listeEntrees());
    for(unsigned int i = 0 ; i < (unsigned int)matieres.size() ; i++)
    {
        if (m_fichierMatieres->chercherValeurParTypeLigneDansEntree(matieres.at(i), Fichier::LigneMatiereNom) == "")
            continue;
        matieresValides.push_back(matieres.at(i));
    }

    unsigned int k(0);
    for(unsigned int i = 0 ; i < colonnes ; i++)
    {
        for(unsigned int j = 0 ; j < lignes ; j++)
        {
            QTableWidgetItem *item(new QTableWidgetItem);
            unsigned int codeMatiere(matieresValides.at(k));
            if (i == 0)
                item->setText(QString::number(codeMatiere));
            else if (i == 1)
            {
                QString designation(m_fichierMatieres->chercherValeurParTypeLigneDansEntree(codeMatiere, Fichier::LigneMatiereNom));
                item->setText(designation);
            }
            else if (i == 2)
            {
                QString coefficient(m_fichierMatieres->chercherValeurParTypeLigneDansEntree(codeMatiere, Fichier::LigneMatiereCoefficient));
                item->setText(coefficient);
            }
            m_liste->setItem(j, i, item);
            k++;
        }
        k = 0;
    }

    m_loListe = new QVBoxLayout(this);
    m_loListe->addWidget(m_titre);
    m_loListe->addWidget(m_liste);
}


FenetreMatieresListe::~FenetreMatieresListe()
{
    delete m_fichierMatieres;
}
