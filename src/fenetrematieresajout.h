#ifndef FENETREMATIERESAJOUT_H
#define FENETREMATIERESAJOUT_H


#include <QDialog>
#include <QLabel>
#include <QSpinBox>
#include <QLineEdit>
#include <QComboBox>
#include "widgets.h"
#include <QFormLayout>
#include <QShowEvent>

#include "fichier.h"


class FenetreMatieresAjout : public QDialog
{
    Q_OBJECT

public:
    FenetreMatieresAjout(QWidget *parent = nullptr);
    ~FenetreMatieresAjout();

private slots:
    void codeMAJ();
    void ajouterMatiere();

private:
    Fichier *m_fichierMatieres;

    Titre *m_titre;

    // wf pour "widget formularire"
    QSpinBox *m_wfCodeMatiere;
    QLineEdit *m_wfDesignation;
    QLineEdit *m_wfCoefficient;

    // wa pour "widget action"
    BoutonMinMin *m_waAjouter;
    BoutonMinMin *m_waAnnuler;

    // lo pour "layout"
    QFormLayout *m_loFormulaireMatieresAjout;

    void showEvent(QShowEvent*);
};


#endif // !FENETREMATIERESAJOUT_H
