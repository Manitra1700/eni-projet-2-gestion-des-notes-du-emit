#ifndef FICHIER_H
#define FICHIER_H

#include <QFile>
#include <QFileInfo>
#include <QList>
#include <QRegularExpression>

extern Q_CORE_EXPORT int qt_ntfs_permission_lookup;

class Fichier
{
public:
    enum TypeFichier {
        FichierEtudiants,
        FichierMatieres,
        FichierNotes
    };

    /* ********************************************************************************************
     * LES ENTREES ET LES LIGNES
     *
     * Chaque fichier est composé de plusieurs groupes de lignes. Les groupes sont appelés "entrée"
     * Une entrée est identifiée par un nombre entier non signé appelé "identifiant"
     * Chaque identifiant est unique à une entrée
     * Toutes les lignes d'une même entrée sont identifiables par l'identifiant de cette entrée
     * Une ligne sert à stocker une information (exemple : le nom d'un étudiant)
     * Pour identifier le type d'information stockée sur une ligne, chaque ligne doit avoir un type
     * Si plusieurs lignes d'une même entrée ont le même type, seule la première ligne est traitée
     * Une ligne est structurée comme suit :
     &identifiant& "type" valeur
     * La valeur doit impérativement tenir sur une ligne, c'est-à-dire que les retours à la ligne
     * ne sont pas autorisées
     * Une ligne qui ne respecte pas la syntaxe ci-dessus ne sera pas prise en compte.
     * *******************************************************************************************/

    enum TypeLigne {
        LigneEtudiantNom,
        LigneEtudiantAdresse,
        LigneEtudiantSexe,
        LigneEtudiantNiveau,
        LigneEtudiantAnnee,
        LigneMatiereNom,
        LigneMatiereCoefficient,
        LigneNoteNumeroInscription,
        TousLesTypes
    };

    Fichier(const TypeFichier &typeFichier);
    ~Fichier();

    /* ********************************************************************************************
     * VERIFICATIONS
     * *******************************************************************************************/
    // true si le fichier existe dans le disque dur, false sinon
    bool estExistant() const;
    // true si on a la permission de lecture, false sinon
    bool estLisible() const;
    // true si on a la permission d'écriture, false sinon
    bool estModifiable() const;
    // true si on a la permission d'écriture sur le répertoire du fichier, false sinon
    bool repertoireEstModifiable() const;
    // retourne le type de fichier à traiter
    TypeFichier typeFichier() const;

    /* ********************************************************************************************
     * LECTURE
     * *******************************************************************************************/
    // Trouver le nombre d'entrées qui contiennent la valeur spécifiée pour le type de ligne spécifié
    unsigned int trouverNombreEntreesParTypeEtValeurLigne(const QString &typeLigne, const QString &valeur = "") const;
    unsigned int trouverNombreEntreesParTypeEtValeurLigne(const TypeLigne &typeLigne, const QString &valeur = "") const;
    // Trouver le nombre d'entrées dont la ligne spécifiée est définie
    unsigned int trouverNombreEntreesParTypeLigne(const QString &typeLigne) const;
    unsigned int trouverNombreEntreesParTypeLigne(const TypeLigne &typeLigne) const;
    // Trouver le nombre d'entrées qui contiennent la valeur spécifiée pour n'importe quel type de ligne
    unsigned int trouverNombreEntreesParValeurLigne(const QString &valeur = "") const;
    // Trouver le nombre d'entrées dans le fichier
    unsigned int trouverNombreEntrees() const;
    // Faire une liste des entrées qui contiennent la valeur spécifiée pour le type de ligne spécifié
    QList<unsigned int> listeEntreesParTypeEtValeurLigne(const QString &typeLigne, const QString &valeur = "") const;
    QList<unsigned int> listeEntreesParTypeEtValeurLigne(const TypeLigne &typeLigne, const QString &valeur = "") const;
    // Faire une liste des entrées dont la ligne spécifiée est définie
    QList<unsigned int> listeEntreesParTypeLigne(const QString &typeLigne) const;
    QList<unsigned int> listeEntreesParTypeLigne(const TypeLigne &typeLigne) const;
    // Faire une liste des entrées qui contiennent la valeur spécifiée pour n'importe quel type de ligne
    QList<unsigned int> listeEntreesParValeurLigne(const QString &valeur = "") const;
    // Faire la liste de toutes les entrées du fichier
    QList<unsigned int> listeEntrees() const;
    // Cette fonction retourne la valeur qui est contenue sur la ligne spécifiée dans l'entrée spécifiée
    QString chercherValeurParTypeLigneDansEntree(const unsigned &identifiant, const QString &typeLigne) const;
    QString chercherValeurParTypeLigneDansEntree(const unsigned int &identifiant, const TypeLigne &typeLigne) const;
    // Déterminer si le type de ligne spécifié est défini
    bool ligneEstDefinie(const unsigned int &identifiant, const QString &typeLigne) const;
    bool ligneEstDefinie(const unsigned int &identifiant, const TypeLigne &typeLigne) const;
    // Déterminer si l'entrée spécifiée contient la valeur spécifiée
    bool entreeContientValeur(const unsigned int &identifiant, const QString &valeur = "") const;
    // Déterminer si l'entrée spécifiée est définie
    bool entreeEstDefinie(const unsigned int &identifiant) const;

    /* ********************************************************************************************
     * ECRITURE
     * *******************************************************************************************/
    // Ajouter une nouvelle ligne pour l'entrée correspondant à l'identifiant spécifié
    // Si la ligne est déjà définie, l'ajout est annulé et la fonction retourne false
    // Pour écraser une valeur existant, utiliser plutôt modifierLigne()
    bool nouvelleLigne(const unsigned int &identifiant, const QString &typeLigne, const QString &valeur = "");
    bool nouvelleLigne(const unsigned int &identifiant, const TypeLigne &typeLigne, const QString &valeur = "");
    // Modifier la valeur de la ligne spécifiée (identifiée par son type et l'identifiant d'entrée correspondante)
    // Si la ligne n'est pas encore définie, la modification est annulée et la fonction retourne false
    // Pour ajouter une nouvelle ligne, utiliser plutôt nouvelleLigne()
    bool modifierLigne(const unsigned int &identifiant, const QString &typeLigne, const QString &valeur = "");
    bool modifierLigne(const unsigned int &identifiant, const TypeLigne &typeLigne, const QString &valeur = "");
    // Supprimer la ligne spécifiée (identifiée par son type et l'identifiant de l'entrée correspondante)
    // En fait, il s'agit simplement de supprimer la valeur. La ligne reste définie.
    bool supprimerLigne(const unsigned int &identifiant, const QString &typeLigne);
    bool supprimerLigne(const unsigned int &identifiant, const TypeLigne &typeLigne);

private:
    // Contient le nom du fichier qui contiendra la liste des étudiants
    const QString m_nomFichierEtudiants;
    // Contient le nom du fichier qui contiendra la liste des matières
    const QString m_nomFichierMatieres;
    // Contient le nom du fichier qui contiendra la liste des notes
    const QString m_nomFichierNotes;

    /* Les constantes suivantes contiennent les mots-clés définissant les
     * types de lignes dans les entrées de chaque fichier */
    const QString m_ligneEtudiantNom;
    const QString m_ligneEtudiantAdresse;
    const QString m_ligneEtudiantSexe;
    const QString m_ligneEtudiantNiveau;
    const QString m_ligneEtudiantAnnee;
    const QString m_ligneMatiereNom;
    const QString m_ligneMatiereCoefficient;
    const QString m_ligneNoteNumeroInscription;
    const QString m_ligneTousLesTypes;

    QFile *m_fichier;
    QFileInfo *m_infoFichier;
    const TypeFichier m_typeFichier;

    QString motCle(const TypeLigne &typeLigne) const;
    QRegularExpression expressionRecherche(const QString &identifiant,
                                           const QString &attribut,
                                           const QString &valeur) const;
};

#endif // !FICHIER_H
