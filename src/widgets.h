#ifndef WIDGETS_H
#define WIDGETS_H

#include <QPushButton>
#include <QLineEdit>
#include <QLabel>

class BoutonMinMin : public QPushButton
{
    Q_OBJECT

public:
    BoutonMinMin(QWidget *parent = nullptr);
    BoutonMinMin(const QString &texte, QWidget *parent = nullptr);
    ~BoutonMinMin();
};

class BoutonMinMax : public QPushButton
{
    Q_OBJECT

public:
    BoutonMinMax(QWidget *parent = nullptr);
    BoutonMinMax(const QString &texte, QWidget *parent = nullptr);
    ~BoutonMinMax();
};

class BoutonMaxMin : public QPushButton
{
    Q_OBJECT

public:
    BoutonMaxMin(QWidget *parent = nullptr);
    BoutonMaxMin(const QString &texte, QWidget *parent = nullptr);
    ~BoutonMaxMin();
};

class BoutonMaxMax : public QPushButton
{
    Q_OBJECT

public:
    BoutonMaxMax(QWidget *parent = nullptr);
    BoutonMaxMax(const QString &texte, QWidget *parent = nullptr);
    ~BoutonMaxMax();
};

class LineEditEnLectureSeule : public QLineEdit
{
    Q_OBJECT

public:
    LineEditEnLectureSeule(QWidget *parent = nullptr, bool assombrir = true);
    LineEditEnLectureSeule(const QString &texte, QWidget *parent = nullptr, bool assombrir = true);
    ~LineEditEnLectureSeule();

private:
    QPalette *m_palette;
};

class Titre : public QLabel
{
    Q_OBJECT

public:
    Titre(const QString &texte, QWidget *parent = nullptr);
    ~Titre();
};

#endif // !WIDGETS_H
