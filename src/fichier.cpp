#include <QDir>
#include <QRegularExpression>
#include <QRegularExpressionMatch>
#include <QRegularExpressionMatchIterator>
#include <QTextStream>
#include "fichier.h"


/* TO-DO -- Idéalement, nous devrions opérer en binaire et ajouter une en-tête pour chaque fichier
 * afin qu'on puisse vérifier si on travaille bien avec les bons formats de fichier (au cas où
 * l'utilisateur s'amuse à remplacer les fichiers par d'autres fichiers que ce programme ne pourra
 * pas parser correctement, par exemple).
 *
 * TO-DO -- Un système de sauvegarde et de fichiers temporaires pour éviter les pertes de données.
*/
Fichier::Fichier(const TypeFichier &typeFichier)
    : m_nomFichierEtudiants("etudiant.ele"), // .ele pour "emit liste étudiant"
      m_nomFichierMatieres("matieres.elm"), // .elm pour "emit liste matières"
      m_nomFichierNotes("notes.eln"), // .eln pour "emit liste notes"
      m_ligneEtudiantNom("nom"),
      m_ligneEtudiantAdresse("adresse"),
      m_ligneEtudiantSexe("sexe"),
      m_ligneEtudiantNiveau("niveau"),
      m_ligneEtudiantAnnee("annee"),
      m_ligneMatiereNom("désignation"),
      m_ligneMatiereCoefficient("coefficient"),
      m_ligneNoteNumeroInscription("\\d+"),
      m_ligneTousLesTypes("\\w+"),
      m_typeFichier(typeFichier)
{
    if (typeFichier == FichierEtudiants)
        m_fichier = new QFile(m_nomFichierEtudiants);
    else if (typeFichier == FichierMatieres)
        m_fichier = new QFile(m_nomFichierMatieres);
    else if (typeFichier == FichierNotes)
        m_fichier = new QFile(m_nomFichierNotes);
    m_infoFichier = new QFileInfo(*m_fichier);
    m_infoFichier->setCaching(false);
}


Fichier::~Fichier()
{
    delete m_fichier;
    delete m_infoFichier;
}


bool Fichier::estExistant() const
{
    return QFileInfo(*m_fichier).absoluteDir().exists(m_fichier->fileName());
}


bool Fichier::estLisible() const
{
    if (!estExistant())
        return false;
    #ifndef Q_OS_WIN32
        return QFileInfo(*m_fichier).isReadable();
    #else
        qt_ntfs_permission_lookup++;
        bool lisible(QFileInfo(*m_fichier).isReadable());
        qt_ntfs_permission_lookup--;
        return lisible;
    #endif
}


bool Fichier::estModifiable() const
{
    if (!estExistant() || !estLisible())
        return false;
    #ifndef Q_OS_WIN32
        return QFileInfo(*m_fichier).isWritable();
    #else
        qt_ntfs_permission_lookup++;
        bool lisible(QFileInfo(*m_fichier).isWritable());
        qt_ntfs_permission_lookup--;
        return lisible;
    #endif
}


bool Fichier::repertoireEstModifiable() const
{
    QFileInfo infoFichier(QFileInfo(*m_fichier).absolutePath());
    #ifndef Q_OS_WIN32
        return infoFichier.isWritable();
    #else
        qt_ntfs_permission_lookup++;
        bool modifiable(infoFichier.isWritable());
        qt_ntfs_permission_lookup--;
        return modifiable;
#endif
}

Fichier::TypeFichier Fichier::typeFichier() const
{
    return m_typeFichier;
}


QString Fichier::motCle(const TypeLigne &typeLigne) const
{
    if (typeLigne == TousLesTypes)
        return m_ligneTousLesTypes;
    if (typeLigne == LigneEtudiantNom)
        return m_ligneEtudiantNom;
    if (typeLigne == LigneEtudiantAdresse)
        return m_ligneEtudiantAdresse;
    if (typeLigne == LigneEtudiantSexe)
        return m_ligneEtudiantSexe;
    if (typeLigne == LigneEtudiantNiveau)
        return m_ligneEtudiantNiveau;
    if (typeLigne == LigneEtudiantAnnee)
        return m_ligneEtudiantAnnee;
    if (typeLigne == LigneMatiereNom)
        return m_ligneMatiereNom;
    if (typeLigne == LigneMatiereCoefficient)
        return m_ligneMatiereCoefficient;
    if (typeLigne == LigneNoteNumeroInscription)
        return m_ligneNoteNumeroInscription;
    return "";
}


QRegularExpression Fichier::expressionRecherche(const QString &identifiant,
                                                const QString &typeLigne,
                                                const QString &valeur) const
{
    return QRegularExpression("^[ \\t]*&" + identifiant +
                              "&[ \\t]+\"" + typeLigne +
                              "\"[ \\t]*" + valeur + "[ \\t]*.*$",
                              QRegularExpression::MultilineOption | QRegularExpression::CaseInsensitiveOption);
}


unsigned int Fichier::trouverNombreEntreesParTypeEtValeurLigne(const QString &typeLigne, const QString &valeur) const
{
    if (!estLisible())
        return 0;
    if (!m_fichier->open(QIODevice::Text | QIODevice::ReadOnly))
        return 0;
    QTextStream flux(m_fichier);
    QString contenu(flux.readAll());
    m_fichier->close();

    QRegularExpression expression(expressionRecherche("(\\d+)", typeLigne, valeur));
    QRegularExpressionMatchIterator iterateur(expression.globalMatch(contenu));

    QList<int> identifiants;
    while(iterateur.hasNext())
    {
        QRegularExpressionMatch resultat(iterateur.next());
        unsigned int identifiant(resultat.captured(1).toInt());
        if (identifiant <= 0)
            continue;
        if (identifiants.contains(identifiant))
            continue;
        identifiants.push_back(identifiant);
    }

    return identifiants.size();
}


unsigned int Fichier::trouverNombreEntreesParTypeEtValeurLigne(const TypeLigne &typeLigne, const QString &valeur) const
{
    return trouverNombreEntreesParTypeEtValeurLigne(motCle(typeLigne), valeur);
}


unsigned int Fichier::trouverNombreEntreesParTypeLigne(const QString &typeLigne) const
{
    return trouverNombreEntreesParTypeEtValeurLigne(typeLigne);
}


unsigned int Fichier::trouverNombreEntreesParTypeLigne(const TypeLigne &typeLigne) const
{
    return trouverNombreEntreesParTypeLigne(motCle(typeLigne));
}


unsigned int Fichier::trouverNombreEntreesParValeurLigne(const QString &valeur) const
{
    return trouverNombreEntreesParTypeEtValeurLigne(TousLesTypes, valeur);
}


unsigned int Fichier::trouverNombreEntrees() const
{
    return trouverNombreEntreesParValeurLigne();
}


QList<unsigned int> Fichier::listeEntreesParTypeEtValeurLigne(const QString &typeLigne, const QString &valeur) const
{
    QList<unsigned int> identifiants;

    if (!estLisible())
        return identifiants;
    if (!m_fichier->open(QIODevice::Text | QIODevice::ReadOnly))
        return identifiants;
    QTextStream flux(m_fichier);
    QString contenu(flux.readAll());
    m_fichier->close();

    QRegularExpression expression(expressionRecherche("(\\d+)", typeLigne, valeur));
    QRegularExpressionMatchIterator iterateur(expression.globalMatch(contenu));

    while(iterateur.hasNext())
    {
        QRegularExpressionMatch resultat(iterateur.next());
        unsigned int identifiant(resultat.captured(1).toInt());
        if (identifiant <= 0)
            continue;
        if (identifiants.contains(identifiant))
            continue;
        identifiants.push_back(identifiant);
    }

    return identifiants;
}


QList<unsigned int> Fichier::listeEntreesParTypeEtValeurLigne(const TypeLigne &typeLigne, const QString &valeur) const
{
    return listeEntreesParTypeEtValeurLigne(motCle(typeLigne), valeur);
}


QList<unsigned int> Fichier::listeEntreesParTypeLigne(const QString &typeLigne) const
{
    return listeEntreesParTypeEtValeurLigne(typeLigne);
}


QList<unsigned int> Fichier::listeEntreesParTypeLigne(const TypeLigne &typeLigne) const
{
    return listeEntreesParTypeLigne(motCle(typeLigne));
}


QList<unsigned int> Fichier::listeEntreesParValeurLigne(const QString &valeur) const
{
    return listeEntreesParTypeEtValeurLigne(TousLesTypes, valeur);
}


QList<unsigned int> Fichier::listeEntrees() const
{
    return listeEntreesParValeurLigne();
}


QString Fichier::chercherValeurParTypeLigneDansEntree(const unsigned int &identifiant, const QString &typeLigne) const
{
    if (!estLisible())
        return "";
    if (!m_fichier->open(QIODevice::Text | QIODevice::ReadOnly))
        return "";
    QTextStream flux(m_fichier);
    QString contenu(flux.readAll());
    m_fichier->close();

    QRegularExpression expression(expressionRecherche(QString::number(identifiant), typeLigne, "(.*)"));
    QRegularExpressionMatch resultat(expression.match(contenu));

    if (!resultat.hasMatch())
        return "";

    return resultat.captured(1);
}


QString Fichier::chercherValeurParTypeLigneDansEntree(const unsigned int &identifiant, const TypeLigne &typeLigne) const
{
    return chercherValeurParTypeLigneDansEntree(identifiant, motCle(typeLigne));
}


bool Fichier::ligneEstDefinie(const unsigned int &identifiant, const QString &typeLigne) const
{
    if (!estLisible())
        return "";
    if (!m_fichier->open(QIODevice::Text | QIODevice::ReadOnly))
        return "";
    QTextStream flux(m_fichier);
    QString contenu(flux.readAll());
    m_fichier->close();

    QRegularExpression expression("^[ \\t]*&" + QString::number(identifiant) +
                                  "&[ \\t]+\"" + typeLigne,
                                  QRegularExpression::MultilineOption | QRegularExpression::CaseInsensitiveOption);
    QRegularExpressionMatch resultat(expression.match(contenu));

    return resultat.hasMatch();
}


bool Fichier::ligneEstDefinie(const unsigned int &identifiant, const TypeLigne &typeLigne) const
{
    return ligneEstDefinie(identifiant, motCle(typeLigne));
}


bool Fichier::entreeContientValeur(const unsigned int &identifiant, const QString &valeur) const
{
    return listeEntreesParValeurLigne(valeur).contains(identifiant);
}


bool Fichier::entreeEstDefinie(const unsigned int &identifiant) const
{
    if (!estLisible())
        return false;
    if (!m_fichier->open(QIODevice::Text | QIODevice::ReadOnly))
        return false;
    QTextStream flux(m_fichier);
    QString contenu(flux.readAll());
    m_fichier->close();

    QRegularExpression expression(expressionRecherche(QString::number(identifiant), "\\w+", ".*"));
    QRegularExpressionMatch resultat(expression.match(contenu));

    return resultat.hasMatch();
}


bool Fichier::nouvelleLigne(const unsigned int &identifiant, const QString &typeLigne, const QString &valeur)
{
    if (!estExistant())
    {
        if (!repertoireEstModifiable())
            return false;
        if (!m_fichier->open(QIODevice::Append))
            return false;
         m_fichier->close();
    }
    if (!estLisible())
        return false;
    if (listeEntreesParTypeLigne(typeLigne).contains(identifiant))
        return false;
    if (!m_fichier->open(QIODevice::Append))
        return false;
    QTextStream flux(m_fichier);
    flux << endl << "&" + QString::number(identifiant) + "& \"" + typeLigne + "\" " + valeur << endl;
    m_fichier->close();
    return true;
}


bool Fichier::nouvelleLigne(const unsigned int &identifiant, const TypeLigne &typeLigne, const QString &valeur)
{
    return nouvelleLigne(identifiant, motCle(typeLigne), valeur);
}


bool Fichier::modifierLigne(const unsigned int &identifiant, const QString &typeLigne, const QString &valeur)
{
    if (!estModifiable())
        return false;
    if (!listeEntreesParTypeLigne(typeLigne).contains(identifiant))
        return false;
    if (!m_fichier->open(QIODevice::Text | QIODevice::ReadOnly))
        return false;
    QTextStream fluxLire(m_fichier);
    QString contenu(fluxLire.readAll());
    m_fichier->close();

    QRegularExpression expression(expressionRecherche(QString::number(identifiant), typeLigne, ".*$"));
    contenu.replace(expression, "&" + QString::number(identifiant) + "& \"" + typeLigne + "\" " + valeur);

    if (!m_fichier->open(QFile::WriteOnly | QFile::Truncate))
        return false;
    QTextStream fluxEcrire(m_fichier);
    fluxEcrire << contenu;
    m_fichier->close();

    return true;
}


bool Fichier::modifierLigne(const unsigned int &identifiant, const TypeLigne &typeLigne, const QString &valeur)
{
    return modifierLigne(identifiant, motCle(typeLigne), valeur);
}


bool Fichier::supprimerLigne(const unsigned int &identifiant, const QString &typeLigne)
{
    return modifierLigne(identifiant, typeLigne);
}


bool Fichier::supprimerLigne(const unsigned int &identifiant, const TypeLigne &typeLigne)
{
    return modifierLigne(identifiant, typeLigne);
}

