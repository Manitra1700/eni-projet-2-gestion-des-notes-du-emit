#ifndef FENETREPRINCIPALE_H
#define FENETREPRINCIPALE_H

#include <QMainWindow>
#include <QLabel>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include "widgets.h"

#include "fenetreetudiantsajout.h"
#include "fenetreetudiantsmodif.h"
#include "fenetreetudiantssuppr.h"
#include "fenetreetudiantsliste.h"

#include "fenetrematieresajout.h"
#include "fenetrematieresmodif.h"
#include "fenetrematieressuppr.h"
#include "fenetrematieresliste.h"

#include "fenetrenotesedit.h"
#include "fenetrenotesbulletin.h"
#include "fenetrenotesclassement.h"


class LabelOptions : public QLabel
{
    Q_OBJECT

public:
    LabelOptions(QWidget *parent = nullptr);
    LabelOptions(const QString &texte, QWidget *parent = nullptr);
    ~LabelOptions();
};


class FenetrePrincipale : public QMainWindow
{
    Q_OBJECT

public:
    FenetrePrincipale(const QString &titre = "Gestion des notes du EMIT", QWidget *parent = nullptr);
    ~FenetrePrincipale();

private slots:
    void afficherFenetreEtudiantsAjout();
    void afficherFenetreEtudiantsModif();
    void afficherFenetreEtudiantsSuppr();
    void afficherFenetreEtudiantsListe();

    void afficherFenetreMatieresAjout();
    void afficherFenetreMatieresModif();
    void afficherFenetreMatieresSuppr();
    void afficherFenetreMatieresListe();

    void afficherFenetreNotesEdit();
    void afficherFenetreNotesBulletin();
    void afficherFenetreNotesClassement();

private:
    QLabel *m_lbLogoImage;
    QLabel *m_lbLogoLabel;
    QVBoxLayout *m_loLogo;

    LabelOptions *m_lbOptionsEtudiants;
    BoutonMinMin *m_boutonEtudiantsAjout;
    BoutonMinMin *m_boutonEtudiantsModif;
    BoutonMinMin *m_boutonEtudiantsSuppr;
    BoutonMinMin *m_boutonEtudiantsListe;

    LabelOptions *m_lbOptionsMatieres;
    BoutonMinMin *m_boutonMatieresAjout;
    BoutonMinMin *m_boutonMatieresModif;
    BoutonMinMin *m_boutonMatieresSuppr;
    BoutonMinMin *m_boutonMatieresListe;

    LabelOptions *m_lbOptionsNotes;
    BoutonMinMin *m_boutonNotesEdition;
    BoutonMinMin *m_boutonNotesBulletin;
    BoutonMinMin *m_boutonNotesClassement;

    LabelOptions *m_lbOptionsQuitter;
    BoutonMinMin *m_boutonQuitter;

    QVBoxLayout *m_loOptions;

    QWidget *m_widgetCentral;
    QHBoxLayout *m_loPrincipal;

    FenetreEtudiantsAjout *m_fenetreEtudiantsAjout;
    FenetreEtudiantsModif *m_fenetreEtudiantsModif;
    FenetreEtudiantsSuppr *m_fenetreEtudiantsSuppr;
    FenetreEtudiantsListe *m_fenetreEtudiantsListe;

    FenetreMatieresAjout *m_fenetreMatieresAjout;
    FenetreMatieresModif *m_fenetreMatieresModif;
    FenetreMatieresSuppr *m_fenetreMatieresSuppr;
    FenetreMatieresListe *m_fenetreMatieresListe;

    FenetreNotesEdit *m_fenetreNotesEdit;
    FenetreNotesBulletin *m_fenetreNotesBulletin;
    FenetreNotesClassement *m_fenetreNotesClassement;
};


#endif // !FENETREPRINCIPALE_H
