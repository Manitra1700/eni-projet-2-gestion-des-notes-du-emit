#include <QDate>
#include <QMessageBox>
#include "fenetrematieresajout.h"


FenetreMatieresAjout::FenetreMatieresAjout(QWidget *parent)
    : QDialog(parent), m_fichierMatieres(new Fichier(Fichier::FichierMatieres))
{
    setAttribute(Qt::WA_DeleteOnClose);
    setWindowTitle("Ajout d'une nouvelle EC");
    resize(650, 360);

    m_titre = new Titre("Ajout d'une nouvelle EC", this);

    unsigned int dernierCodeMatiere(m_fichierMatieres->trouverNombreEntrees());

    m_wfCodeMatiere = new QSpinBox(this);
    m_wfCodeMatiere->setMinimum(1);
    m_wfCodeMatiere->setMaximum(dernierCodeMatiere + 1);
    connect(m_wfCodeMatiere, SIGNAL(valueChanged(int)), this, SLOT(codeMAJ()));

    m_wfDesignation = new QLineEdit(this);
    m_wfCoefficient = new QLineEdit(this);

    m_waAjouter = new BoutonMinMin("Ajouter", this);
    connect(m_waAjouter, SIGNAL(clicked()), this, SLOT(ajouterMatiere()));
    m_waAnnuler = new BoutonMinMin("Annuler", this);
    connect(m_waAnnuler, SIGNAL(clicked()), this, SLOT(close()));

    m_loFormulaireMatieresAjout = new QFormLayout(this);
    m_loFormulaireMatieresAjout->addWidget(m_titre);
    m_loFormulaireMatieresAjout->addRow("Code Matière", m_wfCodeMatiere);
    m_loFormulaireMatieresAjout->addRow("Désignation", m_wfDesignation);
    m_loFormulaireMatieresAjout->addRow("Coefficient", m_wfCoefficient);
    m_loFormulaireMatieresAjout->addWidget(m_waAjouter);
    m_loFormulaireMatieresAjout->addWidget(m_waAnnuler);
}


FenetreMatieresAjout::~FenetreMatieresAjout()
{
    delete m_fichierMatieres;
}


void FenetreMatieresAjout::codeMAJ()
{
    // i pour input - il s'agit du nombre saisi dans le spin box
    unsigned int iCodeMatiere(m_wfCodeMatiere->value());
    // f pour fichier - on va chercher dans le fichier si la valeur de iCodeMatiere a une ligne de désignation
    // et on met cette désignation dans fDesignationCorrespondante.
    // La méthode retourne "" s'il n'y a pas de désignation (matière supprimée ou inexistante)
    QString fDesignationCorrespondante(m_fichierMatieres->chercherValeurParTypeLigneDansEntree(iCodeMatiere, Fichier::LigneMatiereNom));
    if (m_fichierMatieres->entreeEstDefinie(iCodeMatiere))
    {
        if (fDesignationCorrespondante == "")
        {
            m_wfDesignation->setText("Matière supprimée");
            m_wfCoefficient->setText("");
        }
        else
        {
            m_wfDesignation->setText(fDesignationCorrespondante);
            m_wfCoefficient->setText(m_fichierMatieres->chercherValeurParTypeLigneDansEntree(iCodeMatiere, Fichier::LigneMatiereCoefficient));
        }
    }
    else
    {
        m_wfDesignation->setText("");
        m_wfCoefficient->setText("");
    }
}


void FenetreMatieresAjout::ajouterMatiere()
{
    // i pour input - il s'agit du nombre saisi dans le spin box
    unsigned int iCodeMatiere(m_wfCodeMatiere->value());
    if (m_fichierMatieres->entreeEstDefinie(iCodeMatiere))
    {
        QMessageBox::warning(this, "Numéro déjà attribué",
                             "Le numéro " + QString::number(iCodeMatiere) + " est déjà attribué à " +
                             m_fichierMatieres->chercherValeurParTypeLigneDansEntree(iCodeMatiere, Fichier::LigneMatiereNom) +
                             "\nSi vous souhaitez modifier les informations concernant une matière, "
                             "allez plutôt dans l'option 'Modification d'une EC'.");
        return;
    }

    QString iDesignation(m_wfDesignation->text());
    if (iDesignation == "")
    {
        QMessageBox::warning(this, "Désignation requise",
                             "Le champ 'Désignation' est vide. Veuillez le remplir s'il vous plaît.");
        return;
    }

    QString iCoefficient(m_wfCoefficient->text());
    if (iCoefficient == "" || iCoefficient.toDouble() <= 0)
    {
        QMessageBox::warning(this, "Coefficient invalide",
                             "Le champ 'Coefficient' est vide ou invalide. Veuillez le corriger s'il vous plaît.");
        return;
    }

    m_fichierMatieres->nouvelleLigne(iCodeMatiere, Fichier::LigneMatiereNom, iDesignation);
    m_fichierMatieres->nouvelleLigne(iCodeMatiere, Fichier::LigneMatiereCoefficient, iCoefficient);

    int reponse = QMessageBox::information(this, "Ajout effectué", iDesignation +
                                        " a été ajouté avec succès dans la liste des EC. Voudriez-vous ajouter une autre EC ?",
                          QMessageBox::Yes | QMessageBox::No);

    if (reponse == QMessageBox::Yes)
    {
        m_wfCodeMatiere->setMaximum(iCodeMatiere + 1);
        m_wfCodeMatiere->setValue(iCodeMatiere + 1);
    }
    else
    {
        close();
    }
}


void FenetreMatieresAjout::showEvent(QShowEvent*)
{
    codeMAJ();
}
