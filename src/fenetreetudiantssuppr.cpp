#include <QDate>
#include <QMessageBox>
#include "fenetreetudiantssuppr.h"


FenetreEtudiantsSuppr::FenetreEtudiantsSuppr(QWidget *parent)
    : QDialog(parent), m_fichierEtudiants(new Fichier(Fichier::FichierEtudiants))
{
    setAttribute(Qt::WA_DeleteOnClose);
    setWindowTitle("Suppression d'un étudiant");
    resize(650, 360);

    m_titre = new Titre("Suppression d'un étudiant", this);

    unsigned int dernierNumeroInscription(m_fichierEtudiants->trouverNombreEntrees());

    m_wfNumeroInscription = new QSpinBox(this);
    m_wfNumeroInscription->setMinimum(1);
    m_wfNumeroInscription->setMaximum(dernierNumeroInscription);
    connect(m_wfNumeroInscription, SIGNAL(valueChanged(int)), this, SLOT(numeroMAJ()));

    m_wfNom = new LineEditEnLectureSeule(this);
    m_wfAdresse = new LineEditEnLectureSeule(this);
    m_wfSexe = new LineEditEnLectureSeule(this);
    m_wfNiveau = new LineEditEnLectureSeule(this);
    m_wfAnnee = new LineEditEnLectureSeule(this);

    m_waSupprimer = new BoutonMinMin("Supprimer", this);
    connect(m_waSupprimer, SIGNAL(clicked()), this, SLOT(supprimerEtudiant()));
    m_waAnnuler = new BoutonMinMin("Annuler", this);
    connect(m_waAnnuler, SIGNAL(clicked()), this, SLOT(close()));

    m_loFormulaireEtudiantsSuppr = new QFormLayout(this);
    m_loFormulaireEtudiantsSuppr->addWidget(m_titre);
    m_loFormulaireEtudiantsSuppr->addRow("Numéro d'inscription", m_wfNumeroInscription);
    m_loFormulaireEtudiantsSuppr->addRow("Nom", m_wfNom);
    m_loFormulaireEtudiantsSuppr->addRow("Adressse", m_wfAdresse);
    m_loFormulaireEtudiantsSuppr->addRow("Sexe", m_wfSexe);
    m_loFormulaireEtudiantsSuppr->addRow("Niveau", m_wfNiveau);
    m_loFormulaireEtudiantsSuppr->addRow("Année", m_wfAnnee);
    m_loFormulaireEtudiantsSuppr->addWidget(m_waSupprimer);
    m_loFormulaireEtudiantsSuppr->addWidget(m_waAnnuler);
}


FenetreEtudiantsSuppr::~FenetreEtudiantsSuppr()
{
    delete m_fichierEtudiants;
}


void FenetreEtudiantsSuppr::numeroMAJ()
{
    // i pour input - il s'agit du numéro saisi dans le spin box
    unsigned int iNumero(m_wfNumeroInscription->value());
    // f pour fichier - on va chercher dans le fichier si la valeur de iNumero a une ligne de nom
    // et on met ce nom dans fNomCorrespondant. La méthode retourne "" s'il n'y a pas de nom (étudiant supprimé ou non inscrit)
    QString fNomCorrespondant(m_fichierEtudiants->chercherValeurParTypeLigneDansEntree(iNumero, Fichier::LigneEtudiantNom));
    if (m_fichierEtudiants->entreeEstDefinie(iNumero))
    {
        if (fNomCorrespondant == "")
        {
            m_wfNom->setText("Etudiant supprimé");
            m_wfAdresse->setText("");
            m_wfSexe->setText("");
            m_wfNiveau->setText("");
            m_wfAnnee->setText("");
        }
        else
        {
            m_wfNom->setText(fNomCorrespondant);
            m_wfAdresse->setText(m_fichierEtudiants->chercherValeurParTypeLigneDansEntree(iNumero, Fichier::LigneEtudiantAdresse));
            m_wfSexe->setText(m_fichierEtudiants->chercherValeurParTypeLigneDansEntree(iNumero, Fichier::LigneEtudiantSexe));
            m_wfNiveau->setText(m_fichierEtudiants->chercherValeurParTypeLigneDansEntree(iNumero, Fichier::LigneEtudiantNiveau));
            m_wfAnnee->setText(m_fichierEtudiants->chercherValeurParTypeLigneDansEntree(iNumero, Fichier::LigneEtudiantAnnee));
        }
    }
    else
    {
        m_wfNom->setText("");
        m_wfAdresse->setText("");
        m_wfSexe->setText("");
        m_wfNiveau->setText("");
        m_wfAnnee->setText("");
    }
}


void FenetreEtudiantsSuppr::supprimerEtudiant()
{
    // i pour input - il s'agit du numéro saisi dans le spin box
    unsigned int iNumero(m_wfNumeroInscription->value());
    if (!m_fichierEtudiants->entreeEstDefinie(iNumero))
    {
        // Normalement, ceci ne devrait jamais arriver.
        QMessageBox::warning(this, "Numéro non attribué",
                             "Le numéro " + QString::number(iNumero) + " n'est attribué à aucun étudiant dans la liste des étudiants.");
        return;
    }

    QString iNom(m_wfNom->text());
    if (iNom == "")
    {
        QMessageBox::information(this, "Déjà supprimé", "L'étudiant qui correspond au numéro " + QString::number(iNumero) +
                                 " est déjà supprimé de la liste des étudiants.");
        return;
    }

    m_fichierEtudiants->supprimerLigne(iNumero, Fichier::LigneEtudiantNom);
    m_fichierEtudiants->supprimerLigne(iNumero, Fichier::LigneEtudiantAdresse);
    m_fichierEtudiants->supprimerLigne(iNumero, Fichier::LigneEtudiantSexe);
    m_fichierEtudiants->supprimerLigne(iNumero, Fichier::LigneEtudiantNiveau);
    m_fichierEtudiants->supprimerLigne(iNumero, Fichier::LigneEtudiantAnnee);

    QMessageBox::information(this, "Suppression effectuée", "Les informations sur " + iNom +
                                        " ont été effacées de la liste des étudiants.");

    if (iNumero < (unsigned int)m_wfNumeroInscription->maximum())
        m_wfNumeroInscription->setValue(iNumero + 1);
    else
        close();
}


void FenetreEtudiantsSuppr::showEvent(QShowEvent*)
{
    numeroMAJ();
}
