#include <QApplication>
#include <QTranslator>
#include <QLocale>
#include <QLibraryInfo>
#include "fenetreprincipale.h"

int main(int nbrArguments, char *listeArguments[])
{
    QApplication app(nbrArguments, listeArguments);

    QTranslator translator;
    translator.load(QString("qt_fr"),
    QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    app.installTranslator(&translator);

    FenetrePrincipale fenetrePrincipale;
    fenetrePrincipale.show();

    return app.exec();
}
