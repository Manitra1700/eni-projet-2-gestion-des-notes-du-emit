#ifndef FENETREETUDIANTSMODIF_H
#define FENETREETUDIANTSMODIF_H


#include <QDialog>
#include <QLabel>
#include <QSpinBox>
#include <QLineEdit>
#include <QComboBox>
#include "widgets.h"
#include <QFormLayout>
#include <QShowEvent>

#include "fichier.h"


class FenetreEtudiantsModif : public QDialog
{
    Q_OBJECT

public:
    FenetreEtudiantsModif(QWidget *parent = nullptr);
    ~FenetreEtudiantsModif();

private slots:
    void numeroMAJ();
    void modifierEtudiant();

private:
    Fichier *m_fichierEtudiants;

    Titre *m_titre;

    // wf pour "widget formularire"
    QSpinBox *m_wfNumeroInscription;
    QLineEdit *m_wfNom;
    QLineEdit *m_wfAdresse;
    QComboBox *m_wfSexe;
    QComboBox *m_wfNiveau;
    QComboBox *m_wfAnnee;

    // wa pour "widget action"
    BoutonMinMin *m_waModifier;
    BoutonMinMin *m_waAnnuler;

    // lo pour "layout"
    QFormLayout *m_loFormulaireEtudiantsModif;

    void showEvent(QShowEvent*);
};


#endif // !FENETREETUDIANTSMODIF_H
