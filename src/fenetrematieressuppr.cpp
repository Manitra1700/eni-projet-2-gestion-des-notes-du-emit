#include <QDate>
#include <QMessageBox>
#include "fenetrematieressuppr.h"


FenetreMatieresSuppr::FenetreMatieresSuppr(QWidget *parent)
    : QDialog(parent), m_fichierMatieres(new Fichier(Fichier::FichierMatieres))
{
    setAttribute(Qt::WA_DeleteOnClose);
    setWindowTitle("Suppression d'une EC");
    resize(650, 360);

    m_titre = new Titre("Suppression d'une EC", this);

    unsigned int dernierCodeMatiere(m_fichierMatieres->trouverNombreEntrees());

    m_wfCodeMatiere = new QSpinBox(this);
    m_wfCodeMatiere->setMinimum(1);
    m_wfCodeMatiere->setMaximum(dernierCodeMatiere);
    connect(m_wfCodeMatiere, SIGNAL(valueChanged(int)), this, SLOT(codeMAJ()));

    m_lsDesignation = new LineEditEnLectureSeule(this);
    m_lsCoefficient = new LineEditEnLectureSeule(this);

    m_waSupprimer = new BoutonMinMin("Supprimer", this);
    connect(m_waSupprimer, SIGNAL(clicked()), this, SLOT(supprimerMatiere()));
    m_waAnnuler = new BoutonMinMin("Annuler", this);
    connect(m_waAnnuler, SIGNAL(clicked()), this, SLOT(close()));

    m_loFormulaireMatieresSuppr = new QFormLayout(this);
    m_loFormulaireMatieresSuppr->addWidget(m_titre);
    m_loFormulaireMatieresSuppr->addRow("Code matière", m_wfCodeMatiere);
    m_loFormulaireMatieresSuppr->addRow("Désignation", m_lsDesignation);
    m_loFormulaireMatieresSuppr->addRow("Coefficient", m_lsCoefficient);
    m_loFormulaireMatieresSuppr->addWidget(m_waSupprimer);
    m_loFormulaireMatieresSuppr->addWidget(m_waAnnuler);
}


FenetreMatieresSuppr::~FenetreMatieresSuppr()
{
    delete m_fichierMatieres;
}


void FenetreMatieresSuppr::codeMAJ()
{
    // i pour input - il s'agit du nombre saisi dans le spin box
    unsigned int iCodeMatiere(m_wfCodeMatiere->value());
    // f pour fichier - on va chercher dans le fichier si la valeur de iCodeMatiere a une ligne de désignation
    // et on met cette désignation dans fDesignationCorrespondante.
    // La méthode retourne "" s'il n'y a pas de désignation (matière supprimée ou inexistante)
    QString fDesignationCorrespondante(m_fichierMatieres->chercherValeurParTypeLigneDansEntree(iCodeMatiere, Fichier::LigneMatiereNom));
    if (m_fichierMatieres->entreeEstDefinie(iCodeMatiere))
    {
        if (fDesignationCorrespondante == "")
        {
            m_lsDesignation->setText("Matière supprimée");
            m_lsCoefficient->setText("");
        }
        else
        {
            m_lsDesignation->setText(fDesignationCorrespondante);
            m_lsCoefficient->setText(m_fichierMatieres->chercherValeurParTypeLigneDansEntree(iCodeMatiere, Fichier::LigneMatiereCoefficient));
        }
    }
    else
    {
        m_lsDesignation->setText("");
        m_lsCoefficient->setText("");
    }
}


void FenetreMatieresSuppr::supprimerMatiere()
{
    // i pour input - il s'agit du nombre saisi dans le spin box
    unsigned int iCodeMatiere(m_wfCodeMatiere->value());
    if (!m_fichierMatieres->entreeEstDefinie(iCodeMatiere))
    {
        // Normalement, ceci ne devrait jamais arriver.
        QMessageBox::warning(this, "Code non attribué",
                             "Le code " + QString::number(iCodeMatiere) + " n'est attribué à aucune EC dans la liste des EC.");
        return;
    }

    QString iDesignation(m_lsDesignation->text());
    if (iDesignation == "")
    {
        QMessageBox::information(this, "Déjà supprimé", "L'EC qui correspond au code " + QString::number(iCodeMatiere) +
                                 " est déjà supprimée de la liste des EC.");
        return;
    }

    m_fichierMatieres->supprimerLigne(iCodeMatiere, Fichier::LigneMatiereNom);
    m_fichierMatieres->supprimerLigne(iCodeMatiere, Fichier::LigneMatiereCoefficient);

    QMessageBox::information(this, "Suppression effectuée", "Les informations sur " + iDesignation +
                                        " ont été effacées de la liste des EC.");

    if (iCodeMatiere < (unsigned int)m_wfCodeMatiere->maximum())
        m_wfCodeMatiere->setValue(iCodeMatiere + 1);
    else
        close();
}


void FenetreMatieresSuppr::showEvent(QShowEvent*)
{
    codeMAJ();
}
