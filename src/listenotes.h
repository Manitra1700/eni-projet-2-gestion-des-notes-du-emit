#ifndef LISTENOTES_H
#define LISTENOTES_H

#include "fichier.h"


class ListeNotes : private Fichier
{
public:
    ListeNotes(Fichier *fichierNotes, Fichier *fichierEtudiants, Fichier *fichierMatieres);
    ~ListeNotes();

    double trouverNoteEtudiant(const unsigned int &numeroInscription, const unsigned int &codeMatiere) const;
    double calculerNotePondereeEtudiant(const unsigned int &numeroInscription, const unsigned int &codeMatiere) const;
    double calculerTotalNotesPondereesEtudiant(const unsigned int &numeroInscription) const;
    double calculerMoyenneGeneraleEtudiant(const unsigned int &numeroInscription) const;
    QString determinerObservationEtudiant(const unsigned int &numeroInscription) const;

private:
    Fichier *m_fichierEtudiants;
    Fichier *m_fichierMatieres;
    Fichier *m_fichierNotes;

    bool listeEstValide() const;
};

#endif // !FICHIERNOTES_H
