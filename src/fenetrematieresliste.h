#ifndef FENETREMATIERESLISTE_H
#define FENETREMATIERESLISTE_H


#include <QDialog>
#include <QLabel>
#include "widgets.h"
#include <QVBoxLayout>
#include <QTableWidget>
#include "fichier.h"


class FenetreMatieresListe : public QDialog
{
    Q_OBJECT

public:
    FenetreMatieresListe(QWidget *parent = nullptr);
    ~FenetreMatieresListe();

private:
    Fichier *m_fichierMatieres;

    Titre *m_titre;

    QTableWidget *m_liste;

    // lo pour "layout"
    QVBoxLayout *m_loListe;
};


#endif // !FENETREMATIERESLISTE_H
