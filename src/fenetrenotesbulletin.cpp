#include <QApplication>
#include <QMainWindow>
#include <QMessageBox>
#include <QFile>
#include <QDir>
#include <QWebEngineView>
#include <QWebChannel>
#include <QTextDocument>
#include <QPainter>
#include <QPdfWriter>
#include <QPrinter>
#include "fenetrenotesbulletin.h"
#include "communicator.h"

FenetreNotesBulletin::FenetreNotesBulletin(QWidget *parent)
    : QDialog(parent),
      m_fichierEtudiant(new Fichier(Fichier::FichierEtudiants)),
      m_fichierMatieres(new Fichier(Fichier::FichierMatieres)),
      m_fichierNotes(new Fichier(Fichier::FichierNotes)),
      m_listeNotes(new ListeNotes(m_fichierNotes, m_fichierEtudiant, m_fichierMatieres))
{
    setAttribute(Qt::WA_DeleteOnClose);
    setFixedHeight(155);
    resize(860, this->height());
    setWindowTitle("Edition d'un bulletin des notes");

    unsigned int nombreEtudiants(m_fichierEtudiant->trouverNombreEntrees());
    unsigned int nombreMatieres(m_fichierMatieres->trouverNombreEntrees());

    m_titre = new Titre("Edition d'un bulletin des notes", this);
    m_filtreNumeroInscription = new QSpinBox(this);
    m_filtreNumeroInscription->setMinimum(1);
    m_filtreNumeroInscription->setMaximum(nombreEtudiants);
    connect(m_filtreNumeroInscription, SIGNAL(valueChanged(int)), this, SLOT(numeroMAJ()));
    m_actionAfficherApercu = new BoutonMinMin(this);
    m_actionAfficherApercu->setText("Afficher aperçu");
    connect(m_actionAfficherApercu, SIGNAL(clicked()), this, SLOT(afficherApercu()));
    m_lsNotification = new LineEditEnLectureSeule(this, false);
    m_actionFermer = new BoutonMinMin(this);
    m_actionFermer->setText("Retour");
    connect(m_actionFermer, SIGNAL(clicked()), this, SLOT(close()));

    m_loFiltre = new QFormLayout(nullptr);
    m_loFiltre->addRow("N° Inscription", m_filtreNumeroInscription);
    m_loFiltre->addRow("Etudiant correspondant", m_lsNotification);
    m_loFiltre->addWidget(m_actionAfficherApercu);
    m_loFiltre->addWidget(m_actionFermer);

    m_loPrincipal = new QVBoxLayout(nullptr);
    m_loPrincipal->addWidget(m_titre);
    m_loPrincipal->addLayout(m_loFiltre);
    setLayout(m_loPrincipal);

    if (nombreEtudiants <= 0)
    {
        QMessageBox::information(this, "Aucun étudiant", "Il n'y a aucun étudiant dans la liste des étudiants.");
        close();
        return;
    }
    if (nombreMatieres <= 0)
    {
        QMessageBox::information(this, "Aucune EC", "Il n'y a aucune EC dans la liste des EC.");
        close();
        return;
    }

}

FenetreNotesBulletin::~FenetreNotesBulletin()
{
    delete m_fichierEtudiant;
    delete m_fichierMatieres;
    delete m_fichierNotes;
    delete m_listeNotes;
    delete m_apercuOuvert;
}

void FenetreNotesBulletin::desactiverBoutonsActions()
{
    m_actionAfficherApercu->setVisible(false);
    m_actionFermer->setVisible(false);
}

void FenetreNotesBulletin::activerBoutonsActions()
{
    m_actionAfficherApercu->setVisible(true);
    m_actionFermer->setVisible(true);
}


void FenetreNotesBulletin::changerEtatApercu(bool ouvert)
{
    *m_apercuOuvert = ouvert;
}


bool FenetreNotesBulletin::etatApercu() const
{
    return *m_apercuOuvert;
}

void FenetreNotesBulletin::numeroMAJ()
{
    unsigned int iNumero(m_filtreNumeroInscription->value());
    // Nom correspondant à ce numéro :
    QString fNomCorrespondant(m_fichierEtudiant->chercherValeurParTypeLigneDansEntree(iNumero, Fichier::LigneEtudiantNom));
    if (fNomCorrespondant == "")
    {
        m_actionAfficherApercu->setVisible(false);
        m_lsNotification->setText("Etudiant supprimé ou non inscrit");
        return;
    }
    m_lsNotification->setText(fNomCorrespondant);
}

void FenetreNotesBulletin::afficherApercu()
{
    unsigned int numeroInscription(m_filtreNumeroInscription->value());
    QString nomEtudiant(m_fichierEtudiant->chercherValeurParTypeLigneDansEntree(numeroInscription, Fichier::LigneEtudiantNom));
    if (nomEtudiant == "")
    {
        QMessageBox::warning(this, "Etudiant inexistant", "Aucun étudiant ne correspond à ce numéro."
" Cet étudiant a probablement été supprimé de la liste ou n'a pas encore été inscrit.");
        return;
    }
    QString tableauNotes(
"<tr>"
    "<td style=\"background-color: #878; padding-right: 5px; padding-left: 5px;\">!designation!</td>"
    "<td style=\"background-color: #cdc; padding-right: 5px; padding-left: 5px;\">!coefficient!</td>"
    "<td style=\"background-color: #878; padding-right: 5px; padding-left: 5px;\">!note!</td>"
    "<td style=\"background-color: #cdc; padding-right: 5px; padding-left: 5px;\">!noteponderee!</td>"
"</tr>"
    );

    QString modeleHtml(
"<html lang=\"fr\">"
    "<body>"
        "<h1 align=\"center\">BULLETIN DES NOTES</h1>"
        "<b>N° Etudiant</b> : !numero!<br/>"
        "<b>Nom</b> : !nom!<br/>"
        "<b>Niveau</b> : !niveau!<br/>"
        "<b>Année</b> : !annee!<br/>"
        "<table style=\"vertical-align: middle\">"
            "<thead>"
                "<tr>"
                    "<th style=\"background-color: #878; padding-right: 5px; padding-left: 5px;\">DESIGNATION</th>"
                    "<th style=\"background-color: #cdc; padding-right: 5px; padding-left: 5px;\">COEFFICIENT</th>"
                    "<th style=\"background-color: #878; padding-right: 5px; padding-left: 5px;\">NOTE</th>"
                    "<th style=\"background-color: #cdc; padding-right: 5px; padding-left: 5px;\">NOTE PONDEREE</th>"
                "</tr>"
            "</thead>"
            "<tbody>"
                "!tableaunotes!"
                "<tr>"
                    "<td></td>"
                    "<td></td>"
                    "<td style=\"background-color: #878; padding-right: 5px; padding-left: 5px;\">MOYENNE</td>"
                    "<td style=\"background-color: #cdc; padding-right: 5px; padding-left: 5px;\">!moyenne!</td>"
                "</tr>"
                "<tr>"
                    "<td></td>"
                    "<td></td>"
                    "<td style=\"background-color: #878; padding-right: 5px; padding-left: 5px;\">OBSERVATION</td>"
                    "<td style=\"background-color: #cdc; padding-right: 5px; padding-left: 5px;\">!observation!</td>"
                "</tr>"
            "</tbody>"
        "</table>"
    "</body>"
"</html>"
    );

    QTextDocument modele;
    QPrinter printer(QPrinter::PrinterResolution);
    printer.setOutputFormat(QPrinter::PdfFormat);
    printer.setPaperSize(QPrinter::A5);

    QString app_path(qApp->applicationDirPath());
#ifdef Q_OS_MACOS
    QDir app_path_dir(app_path);
    app_path_dir.cdUp();
    app_path_dir.cdUp();
    app_path_dir.cdUp();
    app_path = app_path_dir.absolutePath();
#endif
    QDir dir(app_path + "/minified/web/");
    QString pdf_path(dir.relativeFilePath("bulletin.pdf"));
    printer.setOutputFileName(dir.absoluteFilePath("bulletin.pdf"));
    QFile fichierPdf(dir.absoluteFilePath("bulletin.pdf"));

    QString tableauNotesFinal("");
    unsigned int nombreMatieres(m_fichierMatieres->trouverNombreEntrees());
    for(unsigned int i = 1 ; i <= nombreMatieres ; i++)
    {
        QString designation(m_fichierMatieres->chercherValeurParTypeLigneDansEntree(i, Fichier::LigneMatiereNom));
        if (designation == "")
            continue;
        QString coefficient(m_fichierMatieres->chercherValeurParTypeLigneDansEntree(i, Fichier::LigneMatiereCoefficient));
        if (coefficient.toDouble() <= 0)
            continue;
        QString note(QString::number(m_listeNotes->trouverNoteEtudiant(m_filtreNumeroInscription->value(), i)));
        QString notePonderee(QString::number(m_listeNotes->calculerNotePondereeEtudiant(m_filtreNumeroInscription->value(), i)));
        QString copieTableauNotes(tableauNotes);
        copieTableauNotes.replace(QRegularExpression("!designation!"), designation);
        copieTableauNotes.replace(QRegularExpression("!coefficient!"), coefficient);
        copieTableauNotes.replace(QRegularExpression("!note!"), note);
        copieTableauNotes.replace(QRegularExpression("!noteponderee!"), notePonderee);
        tableauNotesFinal += copieTableauNotes;
    }
    modeleHtml.replace(QRegularExpression("!numero!"), QString::number(numeroInscription));
    modeleHtml.replace(QRegularExpression("!nom!"), nomEtudiant);
    QString niveau(m_fichierEtudiant->chercherValeurParTypeLigneDansEntree(numeroInscription, Fichier::LigneEtudiantNiveau));
    modeleHtml.replace(QRegularExpression("!niveau!"), niveau);
    QString annee(m_fichierEtudiant->chercherValeurParTypeLigneDansEntree(numeroInscription, Fichier::LigneEtudiantAnnee));
    modeleHtml.replace(QRegularExpression("!annee!"), annee);
    modeleHtml.replace(QRegularExpression("!tableaunotes!"), tableauNotesFinal);
    QString moyenne(QString::number(m_listeNotes->calculerMoyenneGeneraleEtudiant(numeroInscription)));
    modeleHtml.replace(QRegularExpression("!moyenne!"), moyenne);
    QString observation(m_listeNotes->determinerObservationEtudiant(numeroInscription));
    modeleHtml.replace(QRegularExpression("!observation!"), observation);

    modele.setHtml(modeleHtml);
    fichierPdf.remove();
    modele.print(&printer);

    FenetreApercuBulletin *fenetreApercu(new FenetreApercuBulletin(this, this));

    auto url(QUrl::fromLocalFile(app_path + "/minified/web/viewer.html"));

    m_communicator = new Communicator(fenetreApercu);
    m_communicator->setUrl(pdf_path);

    m_webView = new QWebEngineView(fenetreApercu);

    QWebChannel *channel(new QWebChannel(fenetreApercu));
    channel->registerObject(QStringLiteral("communicator"), m_communicator);
    m_webView->page()->setWebChannel(channel);

    m_webView->load(url);
    fenetreApercu->setCentralWidget(m_webView);
    fenetreApercu->resize(360, 480);
    fenetreApercu->show();
}


void FenetreNotesBulletin::showEvent(QShowEvent *)
{
    numeroMAJ();
}

void FenetreNotesBulletin::closeEvent(QCloseEvent *event)
{
    if (*m_apercuOuvert)
        event->ignore();
}


FenetreApercuBulletin::FenetreApercuBulletin(QWidget *parentWidget, FenetreNotesBulletin *parentFenetre)
    : QMainWindow(parentWidget),
      m_parentFenetre(parentFenetre)
{
    setAttribute(Qt::WA_DeleteOnClose);
    setWindowTitle("Aperçu du bulletin des notes");
}


FenetreApercuBulletin::~FenetreApercuBulletin()
{

}


void FenetreApercuBulletin::showEvent(QShowEvent*)
{
    m_parentFenetre->desactiverBoutonsActions();
    m_parentFenetre->changerEtatApercu(true);
}


void FenetreApercuBulletin::closeEvent(QCloseEvent *)
{
    m_parentFenetre->activerBoutonsActions();
    m_parentFenetre->changerEtatApercu(false);
}

