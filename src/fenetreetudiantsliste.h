#ifndef FENETREETUDIANTSLISTE_H
#define FENETREETUDIANTSLISTE_H


#include <QDialog>
#include <QComboBox>
#include <QLabel>
#include <QLineEdit>
#include "widgets.h"
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QTableWidget>
#include "fichier.h"
#include <QShowEvent>


class FenetreEtudiantsListe : public QDialog
{
    Q_OBJECT

public:
    FenetreEtudiantsListe(QWidget *parent = nullptr);
    ~FenetreEtudiantsListe();

private slots:
    void filtreMAJ();

private:
    bool ouvert;
    Fichier *m_fichierEtudiants;

    Titre *m_titre;

    QComboBox *m_filtreAnnee;
    QComboBox *m_filtreNiveau;

    QLineEdit *m_aide;
    QTableWidget *m_liste;

    // lo pour "layout"
    QVBoxLayout *m_loFiltre;
    QVBoxLayout *m_loListe;

    void showEvent(QShowEvent*);
};


#endif // !FENETREETUDIANTSLISTE_H
