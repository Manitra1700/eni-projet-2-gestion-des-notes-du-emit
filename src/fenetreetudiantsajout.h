#ifndef FENETREETUDIANTSAJOUT_H
#define FENETREETUDIANTSAJOUT_H


#include <QDialog>
#include <QLabel>
#include <QSpinBox>
#include <QLineEdit>
#include <QComboBox>
#include "widgets.h"
#include <QFormLayout>
#include <QShowEvent>

#include "fichier.h"


class FenetreEtudiantsAjout : public QDialog
{
    Q_OBJECT

public:
    FenetreEtudiantsAjout(QWidget *parent = nullptr);
    ~FenetreEtudiantsAjout();

private slots:
    void numeroMAJ();
    void ajouterEtudiant();

private:
    Fichier *m_fichierEtudiants;

    Titre *m_titre;

    // wf pour "widget formularire"
    QSpinBox *m_wfNumeroInscription;
    QLineEdit *m_wfNom;
    QLineEdit *m_wfAdresse;
    QComboBox *m_wfSexe;
    QComboBox *m_wfNiveau;
    QComboBox *m_wfAnnee;

    // wa pour "widget action"
    BoutonMinMin *m_waAjouter;
    BoutonMinMin *m_waAnnuler;

    // lo pour "layout"
    QFormLayout *m_loFormulaireEtudiantsAjout;

    void showEvent(QShowEvent*);
};


#endif // !FENETREETUDIANTSAJOUT_H
