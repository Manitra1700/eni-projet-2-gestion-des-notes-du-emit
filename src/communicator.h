#ifndef COMMUNICATOR_H
#define COMMUNICATOR_H


#include <QObject>


class Communicator : public QObject
{
	Q_OBJECT
	Q_PROPERTY(QString url READ url WRITE setUrl NOTIFY urlChanged)

public:
    explicit Communicator(QObject *parent = nullptr) : QObject(parent) {}

	QString url() const { return m_url; }
	void setUrl(const QString &url);

signals:
	void urlChanged(const QString &);

private:
    QString m_url;
};


#endif // !COMMUNICATOR_H
