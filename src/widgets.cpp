#include "widgets.h"

BoutonMinMin::BoutonMinMin(QWidget *parent)
    : QPushButton(parent)
{
    setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
}

BoutonMinMin::BoutonMinMin(const QString &texte, QWidget *parent)
    : QPushButton(texte, parent)
{
    setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
}

BoutonMinMin::~BoutonMinMin()
{

}

BoutonMinMax::BoutonMinMax(QWidget *parent)
    : QPushButton(parent)
{
    setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Maximum);
}

BoutonMinMax::BoutonMinMax(const QString &texte, QWidget *parent)
    : QPushButton(texte, parent)
{
    setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Maximum);
}

BoutonMinMax::~BoutonMinMax()
{

}

BoutonMaxMin::BoutonMaxMin(QWidget *parent)
    : QPushButton(parent)
{
    setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Minimum);
}

BoutonMaxMin::BoutonMaxMin(const QString &texte, QWidget *parent)
    : QPushButton(texte, parent)
{
    setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Minimum);
}

BoutonMaxMin::~BoutonMaxMin()
{

}

BoutonMaxMax::BoutonMaxMax(QWidget *parent)
    : QPushButton(parent)
{
    setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
}

BoutonMaxMax::BoutonMaxMax(const QString &texte, QWidget *parent)
    : QPushButton(texte, parent)
{
    setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
}

BoutonMaxMax::~BoutonMaxMax()
{

}

LineEditEnLectureSeule::LineEditEnLectureSeule(QWidget *parent, bool assombrir)
    : QLineEdit(parent), m_palette(new QPalette)
{
    setReadOnly(true);
    if (!assombrir)
        return;
    m_palette->setColor(QPalette::Base, Qt::lightGray);
    m_palette->setColor(QPalette::Text, Qt::darkGray);
    setPalette(*m_palette);
}

LineEditEnLectureSeule::LineEditEnLectureSeule(const QString &texte, QWidget *parent, bool assombrir)
    : QLineEdit(texte, parent), m_palette(new QPalette)
{
    setReadOnly(true);
    if (!assombrir)
        return;
    m_palette->setColor(QPalette::Base, Qt::lightGray);
    m_palette->setColor(QPalette::Text, Qt::darkGray);
    setPalette(*m_palette);
}

LineEditEnLectureSeule::~LineEditEnLectureSeule()
{
    delete m_palette;
}

Titre::Titre(const QString &texte, QWidget *parent)
    : QLabel(texte, parent)
{
    setFont(QFont("Segoe UI", 14, 400));
    setAlignment(Qt::AlignHCenter);
}

Titre::~Titre()
{
    
}
