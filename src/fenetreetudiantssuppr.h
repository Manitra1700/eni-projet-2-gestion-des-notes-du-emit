#ifndef FENETREETUDIANTSSUPPR_H
#define FENETREETUDIANTSSUPPR_H


#include <QDialog>
#include <QLabel>
#include <QSpinBox>
#include <QLineEdit>
#include <QComboBox>
#include "widgets.h"
#include <QFormLayout>
#include <QShowEvent>

#include "fichier.h"


class FenetreEtudiantsSuppr : public QDialog
{
    Q_OBJECT

public:
    FenetreEtudiantsSuppr(QWidget *parent = nullptr);
    ~FenetreEtudiantsSuppr();

private slots:
    void numeroMAJ();
    void supprimerEtudiant();

private:
    Fichier *m_fichierEtudiants;

    Titre *m_titre;

    // wf pour "widget formularire"
    QSpinBox *m_wfNumeroInscription;
    LineEditEnLectureSeule *m_wfNom;
    LineEditEnLectureSeule *m_wfAdresse;
    LineEditEnLectureSeule *m_wfSexe;
    LineEditEnLectureSeule *m_wfNiveau;
    LineEditEnLectureSeule *m_wfAnnee;

    // wa pour "widget action"
    BoutonMinMin *m_waSupprimer;
    BoutonMinMin *m_waAnnuler;

    // lo pour "layout"
    QFormLayout *m_loFormulaireEtudiantsSuppr;

    void showEvent(QShowEvent*);
};


#endif // !FENETREETUDIANTSSUPPR_H
