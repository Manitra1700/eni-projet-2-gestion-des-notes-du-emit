#ifndef FENETRENOTESBULLETIN_H
#define FENETRENOTESBULLETIN_H


#include <QMainWindow>
#include <QDialog>
#include <QLineEdit>
#include <QSpinBox>
#include <QFormLayout>
#include <QVBoxLayout>
#include <QList>
#include <QShowEvent>
#include "widgets.h"
#include "fichier.h"
#include "listenotes.h"


class QWebEngineView;
class Communicator;


class FenetreNotesBulletin : public QDialog
{
    Q_OBJECT

public:
    FenetreNotesBulletin(QWidget *parent = nullptr);
    ~FenetreNotesBulletin();

    void desactiverBoutonsActions();
    void activerBoutonsActions();
    void changerEtatApercu(bool ouvert = false);
    bool etatApercu(void) const;

private slots:
    void numeroMAJ();
    void afficherApercu();

private:
    bool *m_apercuOuvert = new bool(false);

    QWebEngineView *m_webView;
    Communicator *m_communicator;

    Titre *m_titre;

    Fichier *m_fichierEtudiant;
    Fichier *m_fichierMatieres;
    Fichier *m_fichierNotes;
    ListeNotes *m_listeNotes;

    QSpinBox *m_filtreNumeroInscription;
    BoutonMinMin *m_actionAfficherApercu;
    LineEditEnLectureSeule *m_lsNotification;
    BoutonMinMin *m_actionFermer;

    QFormLayout *m_loFiltre;
    QVBoxLayout *m_loPrincipal;

    void showEvent(QShowEvent*) override;
    void closeEvent(QCloseEvent *event) override;
};


class FenetreApercuBulletin : public QMainWindow
{
    Q_OBJECT

public:
    FenetreApercuBulletin(QWidget *parentWidget = nullptr, FenetreNotesBulletin *parentFenetre = nullptr);
    ~FenetreApercuBulletin();

private:
    FenetreNotesBulletin *m_parentFenetre;

    void showEvent(QShowEvent*);
    void closeEvent(QCloseEvent*);
};


#endif // !FENETRENOTESBULLETIN_H
