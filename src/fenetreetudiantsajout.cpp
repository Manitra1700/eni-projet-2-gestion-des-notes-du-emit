#include <QDate>
#include <QMessageBox>
#include "fenetreetudiantsajout.h"


FenetreEtudiantsAjout::FenetreEtudiantsAjout(QWidget *parent)
    : QDialog(parent), m_fichierEtudiants(new Fichier(Fichier::FichierEtudiants))
{
    setAttribute(Qt::WA_DeleteOnClose);
    setWindowTitle("Ajout d'un nouvel étudiant");
    resize(650, 360);

    m_titre = new Titre("Ajout d'un nouvel étudiant", this);

    unsigned int dernierNumeroInscription(m_fichierEtudiants->trouverNombreEntrees());

    m_wfNumeroInscription = new QSpinBox(this);
    m_wfNumeroInscription->setMinimum(1);
    m_wfNumeroInscription->setMaximum(dernierNumeroInscription + 1);
    connect(m_wfNumeroInscription, SIGNAL(valueChanged(int)), this, SLOT(numeroMAJ()));

    m_wfNom = new QLineEdit(this);
    m_wfAdresse = new QLineEdit(this);

    m_wfSexe = new QComboBox(this);
    m_wfSexe->addItem("Masculin");
    m_wfSexe->addItem("Féminin");

    m_wfNiveau = new QComboBox(this);
    m_wfNiveau->addItem("L1");
    m_wfNiveau->addItem("L2");
    m_wfNiveau->addItem("L3");
    m_wfNiveau->addItem("M1");
    m_wfNiveau->addItem("M2");

    m_wfAnnee = new QComboBox(this);
    for(auto i = 2000 ; i <= QDate::currentDate().year() ; i++)
        m_wfAnnee->addItem(QString::number(i));

    m_waAjouter = new BoutonMinMin("Ajouter", this);
    connect(m_waAjouter, SIGNAL(clicked()), this, SLOT(ajouterEtudiant()));
    m_waAnnuler = new BoutonMinMin("Annuler", this);
    connect(m_waAnnuler, SIGNAL(clicked()), this, SLOT(close()));

    m_loFormulaireEtudiantsAjout = new QFormLayout(this);
    m_loFormulaireEtudiantsAjout->addWidget(m_titre);
    m_loFormulaireEtudiantsAjout->addRow("Numéro d'inscription", m_wfNumeroInscription);
    m_loFormulaireEtudiantsAjout->addRow("Nom", m_wfNom);
    m_loFormulaireEtudiantsAjout->addRow("Adressse", m_wfAdresse);
    m_loFormulaireEtudiantsAjout->addRow("Sexe", m_wfSexe);
    m_loFormulaireEtudiantsAjout->addRow("Niveau", m_wfNiveau);
    m_loFormulaireEtudiantsAjout->addRow("Année", m_wfAnnee);
    m_loFormulaireEtudiantsAjout->addWidget(m_waAjouter);
    m_loFormulaireEtudiantsAjout->addWidget(m_waAnnuler);
}


FenetreEtudiantsAjout::~FenetreEtudiantsAjout()
{
    delete m_fichierEtudiants;
}


void FenetreEtudiantsAjout::numeroMAJ()
{
    // i pour input - il s'agit du numéro saisi dans le spin box
    unsigned int iNumero(m_wfNumeroInscription->value());
    // f pour fichier - on va chercher dans le fichier si la valeur de iNumero a une ligne de nom
    // et on met ce nom dans fNomCorrespondant. La méthode retourne "" s'il n'y a pas de nom (étudiant supprimé ou non inscrit)
    QString fNomCorrespondant(m_fichierEtudiants->chercherValeurParTypeLigneDansEntree(iNumero, Fichier::LigneEtudiantNom));
    if (m_fichierEtudiants->entreeEstDefinie(iNumero))
    {
        if (fNomCorrespondant == "")
        {
            m_wfNom->setText("Etudiant supprimé");
            m_wfAdresse->setText("");
            m_wfSexe->setCurrentIndex(0);
            m_wfNiveau->setCurrentIndex(0);
            m_wfAnnee->setCurrentIndex(0);
        }
        else
        {
            m_wfNom->setText(fNomCorrespondant);
            m_wfAdresse->setText(m_fichierEtudiants->chercherValeurParTypeLigneDansEntree(iNumero, Fichier::LigneEtudiantAdresse));
            m_wfSexe->setCurrentText(m_fichierEtudiants->chercherValeurParTypeLigneDansEntree(iNumero, Fichier::LigneEtudiantSexe));
            m_wfNiveau->setCurrentText(m_fichierEtudiants->chercherValeurParTypeLigneDansEntree(iNumero, Fichier::LigneEtudiantNiveau));
            m_wfAnnee->setCurrentText(m_fichierEtudiants->chercherValeurParTypeLigneDansEntree(iNumero, Fichier::LigneEtudiantAnnee));
        }
    }
    else
    {
        m_wfNom->setText("");
        m_wfAdresse->setText("");
        m_wfSexe->setCurrentIndex(0);
        m_wfNiveau->setCurrentIndex(0);
        m_wfAnnee->setCurrentIndex(0);
    }
}


void FenetreEtudiantsAjout::ajouterEtudiant()
{
    // i pour input - il s'agit du numéro saisi dans le spin box
    unsigned int iNumero(m_wfNumeroInscription->value());
    if (m_fichierEtudiants->entreeEstDefinie(iNumero))
    {
        QMessageBox::warning(this, "Numéro déjà attribué",
                             "Le numéro " + QString::number(iNumero) + " est déjà attribué à " +
                             m_fichierEtudiants->chercherValeurParTypeLigneDansEntree(iNumero, Fichier::LigneEtudiantNom) +
                             "\nSi vous souhaitez modifier les informations concernant un étudiant, "
                             "allez plutôt dans l'option 'Modification d'un étudiant'.");
        return;
    }

    QString iNom(m_wfNom->text());
    if (iNom == "")
    {
        QMessageBox::warning(this, "Nom requis",
                             "Le champ 'Nom' est vide. Veuillez le remplir s'il vous plaît.");
        return;
    }

    QString iAdresse(m_wfAdresse->text());
    if (iAdresse == "")
    {
        QMessageBox::warning(this, "Adresse requise",
                             "Le champ 'Adresse' est vide. Veuillez le remplir s'il vous plaît.");
        return;
    }

    QString iSexe(m_wfSexe->currentText());
    QString iNiveau(m_wfNiveau->currentText());
    QString iAnnee(m_wfAnnee->currentText());

    m_fichierEtudiants->nouvelleLigne(iNumero, Fichier::LigneEtudiantNom, iNom);
    m_fichierEtudiants->nouvelleLigne(iNumero, Fichier::LigneEtudiantAdresse, iAdresse);
    m_fichierEtudiants->nouvelleLigne(iNumero, Fichier::LigneEtudiantSexe, iSexe);
    m_fichierEtudiants->nouvelleLigne(iNumero, Fichier::LigneEtudiantNiveau, iNiveau);
    m_fichierEtudiants->nouvelleLigne(iNumero, Fichier::LigneEtudiantAnnee, iAnnee);

    int reponse = QMessageBox::information(this, "Inscription effectuée", iNom +
                                        " a été ajouté(e) avec succès dans la liste des étudiants. Voudriez-vous ajouter un(e) autre étudiant(e) ?",
                          QMessageBox::Yes | QMessageBox::No);

    if (reponse == QMessageBox::Yes)
    {
        m_wfNumeroInscription->setMaximum(iNumero + 1);
        m_wfNumeroInscription->setValue(iNumero + 1);
    }
    else
    {
        close();
    }
}


void FenetreEtudiantsAjout::showEvent(QShowEvent*)
{
    numeroMAJ();
}
