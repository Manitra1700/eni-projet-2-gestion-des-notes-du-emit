#include <QDate>
#include <QMessageBox>
#include "fenetrematieresmodif.h"


FenetreMatieresModif::FenetreMatieresModif(QWidget *parent)
    : QDialog(parent), m_fichierMatieres(new Fichier(Fichier::FichierMatieres))
{
    setAttribute(Qt::WA_DeleteOnClose);
    setWindowTitle("Modification d'une EC");
    resize(650, 360);

    m_titre = new Titre("Modification d'une EC", this);

    unsigned int dernierCodeMatiere(m_fichierMatieres->trouverNombreEntrees());

    m_wfCodeMatiere = new QSpinBox(this);
    m_wfCodeMatiere->setMinimum(1);
    m_wfCodeMatiere->setMaximum(dernierCodeMatiere);
    connect(m_wfCodeMatiere, SIGNAL(valueChanged(int)), this, SLOT(codeMAJ()));

    m_wfDesignation = new QLineEdit(this);
    m_wfCoefficient = new QLineEdit(this);

    m_waModifier = new BoutonMinMin("Enregistrer", this);
    connect(m_waModifier, SIGNAL(clicked()), this, SLOT(modifierMatiere()));
    m_waAnnuler = new BoutonMinMin("Annuler", this);
    connect(m_waAnnuler, SIGNAL(clicked()), this, SLOT(close()));

    m_loFormulaireMatieresModif = new QFormLayout(this);
    m_loFormulaireMatieresModif->addWidget(m_titre);
    m_loFormulaireMatieresModif->addRow("Code Matière", m_wfCodeMatiere);
    m_loFormulaireMatieresModif->addRow("Désignation", m_wfDesignation);
    m_loFormulaireMatieresModif->addRow("Coefficient", m_wfCoefficient);
    m_loFormulaireMatieresModif->addWidget(m_waModifier);
    m_loFormulaireMatieresModif->addWidget(m_waAnnuler);
}


FenetreMatieresModif::~FenetreMatieresModif()
{
    delete m_fichierMatieres;
}


void FenetreMatieresModif::codeMAJ()
{
    // i pour input - il s'agit du nombre saisi dans le spin box
    unsigned int iCodeMatiere(m_wfCodeMatiere->value());
    // f pour fichier - on va chercher dans le fichier si la valeur de iCodeMatiere a une ligne de désignation
    // et on met cette désignation dans fDesignationCorrespondante.
    // La méthode retourne "" s'il n'y a pas de désignation (matière supprimée ou inexistante)
    QString fDesignationCorrespondante(m_fichierMatieres->chercherValeurParTypeLigneDansEntree(iCodeMatiere, Fichier::LigneMatiereNom));
    if (m_fichierMatieres->entreeEstDefinie(iCodeMatiere))
    {
        if (fDesignationCorrespondante == "")
        {
            m_wfDesignation->setText("Matière supprimée");
            m_wfCoefficient->setText("");
        }
        else
        {
            m_wfDesignation->setText(fDesignationCorrespondante);
            m_wfCoefficient->setText(m_fichierMatieres->chercherValeurParTypeLigneDansEntree(iCodeMatiere, Fichier::LigneMatiereCoefficient));
        }
    }
    else
    {
        m_wfDesignation->setText("");
        m_wfCoefficient->setText("");
    }
}


void FenetreMatieresModif::modifierMatiere()
{
    // i pour input - il s'agit du nombre saisi dans le spin box
    unsigned int iCodeMatiere(m_wfCodeMatiere->value());
    if (!m_fichierMatieres->entreeEstDefinie(iCodeMatiere))
    {
        // Normalement, ceci ne devrait jamais arriver.
        QMessageBox::warning(this, "Code non attribué",
                             "Le code " + QString::number(iCodeMatiere) + " n'est attribué à aucune EC dans la liste des EC."
                             "\nSi vous souhaitez ajouter une nouvelle EC, allez plutôt dans l'option 'Ajout d'une nouvelle EC'.");
        return;
    }

    QString iDesignation(m_wfDesignation->text());
    if (iDesignation == "")
    {
        QMessageBox::warning(this, "Désignation requise",
                             "Le champ 'Désignation' est vide. Veuillez le remplir s'il vous plaît.");
        return;
    }

    QString iCoefficient(m_wfCoefficient->text());
    if (iCoefficient == "" || iCoefficient.toDouble() <= 0)
    {
        QMessageBox::warning(this, "Coefficient invalide",
                             "Le champ 'Coefficient' est vide ou invalide. Veuillez le corriger s'il vous plaît.");
        return;
    }

    m_fichierMatieres->modifierLigne(iCodeMatiere, Fichier::LigneMatiereNom, iDesignation);
    m_fichierMatieres->modifierLigne(iCodeMatiere, Fichier::LigneMatiereCoefficient, iCoefficient);

    QMessageBox::information(this, "Modification effectuée", "Les informations concernant " + iDesignation +
                                        " ont été modifiées avec succès.");

    if (iCodeMatiere < (unsigned int)m_wfCodeMatiere->maximum())
        m_wfCodeMatiere->setValue(iCodeMatiere + 1);
    else
        close();
}


void FenetreMatieresModif::showEvent(QShowEvent*)
{
    codeMAJ();
}
