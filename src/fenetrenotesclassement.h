#ifndef FENETRENOTESCLASSEMENT_H
#define FENETRENOTESCLASSEMENT_H


#include <QMainWindow>
#include <QDialog>
#include <QComboBox>
#include <QPushButton>
#include <QFormLayout>
#include <QWebEngineView>
#include "fichier.h"
#include "listenotes.h"
#include "communicator.h"


class FenetreNotesClassement : public QDialog
{
    Q_OBJECT

public:
    FenetreNotesClassement(QWidget *parent = nullptr);
    ~FenetreNotesClassement();

    void desactiverBoutonsActions();
    void activerBoutonsActions();
    void changerEtatApercu(bool ouvert = false);
    bool etatApercu(void) const;

private slots:
    void afficherApercu();

private:
    bool *m_apercuOuvert = new bool(false);

    QComboBox *m_filtreAnnee;
    QComboBox *m_filtreNiveau;
    QPushButton *m_actionAfficherApercu;
    QPushButton *m_actionFermer;

    Fichier *m_fichierEtudiant;
    Fichier *m_fichierMatieres;
    Fichier *m_fichierNotes;
    ListeNotes *m_listeNotes;

    Communicator *m_communicator;
    QWebEngineView *m_webView;

    QFormLayout *m_loFiltre;

    void closeEvent(QCloseEvent *event) override;
};


class FenetreApercuClassement : public QMainWindow
{
    Q_OBJECT

public:
    FenetreApercuClassement(QWidget *parentWidget = nullptr, FenetreNotesClassement *parentFenetre = nullptr);
    ~FenetreApercuClassement();

private:
    FenetreNotesClassement *m_parentFenetre;

    void showEvent(QShowEvent*);
    void closeEvent(QCloseEvent*);
};


#endif // !FENETRENOTESCLASSEMENT_H
