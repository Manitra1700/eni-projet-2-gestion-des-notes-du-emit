#include <QApplication>
#include <QLabel>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QWidgetMapper>
#include "fenetreprincipale.h"
#include "fichier.h"
#include "listenotes.h"
#include "widgets.h"


FenetrePrincipale::FenetrePrincipale(const QString &titre, QWidget *parent)
    : QMainWindow(parent)
{
    setWindowTitle(titre);
    resize(860, 480);

    m_lbLogoImage = new QLabel(this);
    m_lbLogoImage->setPixmap(QPixmap("logo_emit.png"));
    m_lbLogoImage->setAlignment(Qt::AlignHCenter | Qt::AlignTop);
    m_lbLogoLabel = new QLabel("Gestion des notes du EMIT", this);
    m_lbLogoLabel->setFont(QFont("Segoe UI", 16, 400));
    m_lbLogoLabel->setAlignment(Qt::AlignHCenter | Qt::AlignTop);
    m_loLogo = new QVBoxLayout;
    m_loLogo->addWidget(m_lbLogoImage);
    m_loLogo->addWidget(m_lbLogoLabel);

    m_lbOptionsEtudiants = new LabelOptions("Etudiants", this);
    m_boutonEtudiantsAjout = new BoutonMinMin("Ajout d'un nouvel étudiant", this);
    connect(m_boutonEtudiantsAjout, SIGNAL(clicked()), this, SLOT(afficherFenetreEtudiantsAjout()));
    m_boutonEtudiantsModif = new BoutonMinMin("Modification d'un étudiant", this);
    connect(m_boutonEtudiantsModif, SIGNAL(clicked()), this, SLOT(afficherFenetreEtudiantsModif()));
    m_boutonEtudiantsSuppr = new BoutonMinMin("Suppression d'un étudiant", this);
    connect(m_boutonEtudiantsSuppr, SIGNAL(clicked()), this, SLOT(afficherFenetreEtudiantsSuppr()));
    m_boutonEtudiantsListe = new BoutonMinMin("Listage des étudiants", this);
    connect(m_boutonEtudiantsListe, SIGNAL(clicked()), this, SLOT(afficherFenetreEtudiantsListe()));

    m_lbOptionsMatieres = new LabelOptions("Matieres", this);
    m_boutonMatieresAjout = new BoutonMinMin("Ajout d'une nouvelle EC", this);
    connect(m_boutonMatieresAjout, SIGNAL(clicked()), this, SLOT(afficherFenetreMatieresAjout()));
    m_boutonMatieresModif = new BoutonMinMin("Modification d'une EC", this);
    connect(m_boutonMatieresModif, SIGNAL(clicked()), this, SLOT(afficherFenetreMatieresModif()));
    m_boutonMatieresSuppr = new BoutonMinMin("Suppression d'une EC", this);
    connect(m_boutonMatieresSuppr, SIGNAL(clicked()), this, SLOT(afficherFenetreMatieresSuppr()));
    m_boutonMatieresListe = new BoutonMinMin("Listage des EC", this);
    connect(m_boutonMatieresListe, SIGNAL(clicked()), this, SLOT(afficherFenetreMatieresListe()));

    m_lbOptionsNotes = new LabelOptions("Notes", this);
    m_boutonNotesEdition = new BoutonMinMin("Edition des notes", this);
    connect(m_boutonNotesEdition, SIGNAL(clicked()), this, SLOT(afficherFenetreNotesEdit()));
    m_boutonNotesBulletin = new BoutonMinMin("Edition d'un bulletin des notes", this);
    connect(m_boutonNotesBulletin, SIGNAL(clicked()), this, SLOT(afficherFenetreNotesBulletin()));
    m_boutonNotesClassement = new BoutonMinMin("Classement par ordre de mérite", this);
    connect(m_boutonNotesClassement, SIGNAL(clicked()), this, SLOT(afficherFenetreNotesClassement()));

    m_lbOptionsQuitter = new LabelOptions("Quitter", this);
    m_boutonQuitter = new BoutonMinMin("Quitter", this);
    connect(m_boutonQuitter, SIGNAL(clicked()), this, SLOT(close()));

    m_loOptions = new QVBoxLayout(nullptr);
    m_loOptions->addWidget(m_lbOptionsEtudiants);
    m_loOptions->addWidget(m_boutonEtudiantsAjout);
    m_loOptions->addWidget(m_boutonEtudiantsModif);
    m_loOptions->addWidget(m_boutonEtudiantsSuppr);
    m_loOptions->addWidget(m_boutonEtudiantsListe);

    m_loOptions->addWidget(m_lbOptionsMatieres);
    m_loOptions->addWidget(m_boutonMatieresAjout);
    m_loOptions->addWidget(m_boutonMatieresModif);
    m_loOptions->addWidget(m_boutonMatieresSuppr);
    m_loOptions->addWidget(m_boutonMatieresListe);

    m_loOptions->addWidget(m_lbOptionsNotes);
    m_loOptions->addWidget(m_boutonNotesEdition);
    m_loOptions->addWidget(m_boutonNotesBulletin);
    m_loOptions->addWidget(m_boutonNotesClassement);

    m_loOptions->addWidget(m_lbOptionsQuitter);
    m_loOptions->addWidget(m_boutonQuitter);

    m_widgetCentral = new QWidget(this);
    m_loPrincipal = new QHBoxLayout(m_widgetCentral);
    m_loPrincipal->addLayout(m_loLogo);
    m_loPrincipal->addLayout(m_loOptions);

    setCentralWidget(m_widgetCentral);
}


LabelOptions::LabelOptions(QWidget *parent)
    : QLabel(parent)
{
    setFont(QFont("Segoe UI", 12, 400));
    setAlignment(Qt::AlignHCenter | Qt::AlignBottom);
}


LabelOptions::LabelOptions(const QString &texte, QWidget *parent)
    : QLabel(texte, parent)
{
    setFont(QFont("Segoe UI", 12, 400));
    setAlignment(Qt::AlignHCenter | Qt::AlignBottom);
}


LabelOptions::~LabelOptions()
{

}


void FenetrePrincipale::afficherFenetreEtudiantsAjout()
{
    m_fenetreEtudiantsAjout = new FenetreEtudiantsAjout(this);
    m_fenetreEtudiantsAjout->open();
}


void FenetrePrincipale::afficherFenetreEtudiantsModif()
{
    m_fenetreEtudiantsModif = new FenetreEtudiantsModif(this);
    m_fenetreEtudiantsModif->open();
}


void FenetrePrincipale::afficherFenetreEtudiantsSuppr()
{
    m_fenetreEtudiantsSuppr = new FenetreEtudiantsSuppr(this);
    m_fenetreEtudiantsSuppr->open();
}


void FenetrePrincipale::afficherFenetreEtudiantsListe()
{
    m_fenetreEtudiantsListe = new FenetreEtudiantsListe(this);
    m_fenetreEtudiantsListe->open();
}

void FenetrePrincipale::afficherFenetreMatieresAjout()
{
    m_fenetreMatieresAjout = new FenetreMatieresAjout(this);
    m_fenetreMatieresAjout->open();
}


void FenetrePrincipale::afficherFenetreMatieresModif()
{
    m_fenetreMatieresModif = new FenetreMatieresModif(this);
    m_fenetreMatieresModif->open();
}


void FenetrePrincipale::afficherFenetreMatieresSuppr()
{
    m_fenetreMatieresSuppr = new FenetreMatieresSuppr(this);
    m_fenetreMatieresSuppr->open();
}


void FenetrePrincipale::afficherFenetreMatieresListe()
{
    m_fenetreMatieresListe = new FenetreMatieresListe(this);
    m_fenetreMatieresListe->open();
}


void FenetrePrincipale::afficherFenetreNotesEdit()
{
    m_fenetreNotesEdit = new FenetreNotesEdit(this);
    m_fenetreNotesEdit->open();
}


void FenetrePrincipale::afficherFenetreNotesBulletin()
{
    m_fenetreNotesBulletin = new FenetreNotesBulletin(this);
    m_fenetreNotesBulletin->open();
}


void FenetrePrincipale::afficherFenetreNotesClassement()
{
    m_fenetreNotesClassement = new FenetreNotesClassement(nullptr);
    m_fenetreNotesClassement->open();
}


FenetrePrincipale::~FenetrePrincipale()
{
    /*
    En fait, toutes ces fenêtres ont l'attribut WA_DeleteOnClose, donc on n'a pas besoin de release.
    Et de toute façon, ce destructeur n'est pas la bonne place pour faire ces delete.

    delete m_fenetreEtudiantsAjout;
    delete m_fenetreEtudiantsModif;
    delete m_fenetreEtudiantsSuppr;
    delete m_fenetreMatieresAjout;
    delete m_fenetreMatieresModif;
    delete m_fenetreMatieresSuppr;
    delete m_fenetreNotesEdit;
    delete m_fenetreNotesBulletin;
    delete m_fenetreNotesClassement;
    */
}

